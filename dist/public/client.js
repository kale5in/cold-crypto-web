(function(FuseBox){FuseBox.$fuse$=FuseBox;
FuseBox.target = "browser";
// allowSyntheticDefaultImports
FuseBox.sdep = true;
FuseBox.pkg("default", {}, function(___scope___){
___scope___.file("client/index.jsx", function(exports, require, module, __filename, __dirname){

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const redux_1 = require("redux");
const emotion_1 = require("emotion");
const react_redux_1 = require("react-redux");
const fuse_react_1 = require("fuse-react");
const redux_saga_1 = tslib_1.__importDefault(require("redux-saga"));
const react_1 = tslib_1.__importStar(require("react"));
const react_dom_1 = tslib_1.__importDefault(require("react-dom"));
const reducers_1 = tslib_1.__importDefault(require("./reducers"));
const sagas_1 = tslib_1.__importDefault(require("./sagas"));
const Home_1 = tslib_1.__importDefault(require("./components/Home"));
const Login_1 = tslib_1.__importDefault(require("./components/Login"));
const txCreation_1 = tslib_1.__importDefault(require("./components/txCreation"));
const Wallet_1 = tslib_1.__importDefault(require("./components/Wallet"));
const WalletList_1 = tslib_1.__importDefault(require("./components/WalletList"));
const WebrtcServer_1 = tslib_1.__importDefault(require("./components/Webrtc/WebrtcServer"));
const TxView_1 = tslib_1.__importDefault(require("./components/TxView"));
const Pay_1 = tslib_1.__importDefault(require("./components/Pay"));
const PayToAddress_1 = tslib_1.__importDefault(require("./components/PayToAddress"));
emotion_1.injectGlobal({
    'html,body,button,input,select': {
        fontFamily: '"Lato", Arial, sans-serif',
        margin: 0,
    }
});
class Root extends react_1.Component {
    render() {
        return (react_1.default.createElement(react_redux_1.Provider, { store: store },
            react_1.default.createElement(fuse_react_1.Switch, null,
                react_1.default.createElement(fuse_react_1.Route, { path: '/', exact: true, component: Home_1.default }),
                react_1.default.createElement(fuse_react_1.Route, { path: '/login', component: Login_1.default }),
                react_1.default.createElement(fuse_react_1.Route, { path: '/wallets', component: WalletList_1.default }),
                react_1.default.createElement(fuse_react_1.Route, { path: '/wallet/:symbol/:address', component: Wallet_1.default }),
                react_1.default.createElement(fuse_react_1.Route, { path: '/txCreation/:blockchain/:address', component: txCreation_1.default }),
                react_1.default.createElement(fuse_react_1.Route, { path: '/webrtc', component: WebrtcServer_1.default }),
                react_1.default.createElement(fuse_react_1.Route, { path: '/tx', component: TxView_1.default }),
                react_1.default.createElement(fuse_react_1.Route, { exact: true, path: '/pay/:address', component: Pay_1.default }),
                react_1.default.createElement(fuse_react_1.Route, { exact: true, path: '/paytoaddress/:address', component: PayToAddress_1.default }))));
    }
}
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || redux_1.compose;
const sagaMiddleware = redux_saga_1.default();
const store = redux_1.createStore(reducers_1.default, composeEnhancers(redux_1.applyMiddleware(sagaMiddleware)));
sagaMiddleware.run(sagas_1.default);
react_dom_1.default.render(react_1.default.createElement(Root, null), document.querySelector('#root'));
//# sourceMappingURL=index.js.map
});
___scope___.file("client/reducers/index.js", function(exports, require, module, __filename, __dirname){

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const redux_1 = require("redux");
const wallet_1 = tslib_1.__importDefault(require("./wallet"));
const webrtc_1 = tslib_1.__importDefault(require("./webrtc"));
exports.default = redux_1.combineReducers({ wallet: wallet_1.default, webrtc: webrtc_1.default });
//# sourceMappingURL=index.js.map
});
___scope___.file("client/reducers/wallet.js", function(exports, require, module, __filename, __dirname){

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const redux_act_1 = require("redux-act");
const actions_1 = require("../actions");
const walletDefaultState = {
    error: null,
    wallets: [],
    payData: {},
};
const walletReducer = redux_act_1.createReducer({}, walletDefaultState);
walletReducer.on(actions_1.addWallets, (_, payload) => {
    const isErr = payload instanceof Error;
    return { ..._,
        error: isErr ? payload.message : null,
        wallets: isErr ? null : payload,
    };
});
walletReducer.on(actions_1.setPayData, (_, payload) => {
    return { ..._,
        payData: payload
    };
});
exports.default = walletReducer;
//# sourceMappingURL=wallet.js.map
});
___scope___.file("client/actions/index.js", function(exports, require, module, __filename, __dirname){

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const redux_act_1 = require("redux-act");
exports.setQr = redux_act_1.createAction('set qr');
exports.generateQr = redux_act_1.createAction('generate qr');
exports.setQrScanned = redux_act_1.createAction('set qr scanned');
exports.setQrCode = redux_act_1.createAction('set qr code');
exports.addWallets = redux_act_1.createAction('add wallets');
exports.scanWallets = redux_act_1.createAction('scan wallets');
exports.scanTransaction = redux_act_1.createAction('scan transaction');
exports.startSendingTx = redux_act_1.createAction('start transaction');
exports.scanAnswer = redux_act_1.createAction('scan answer');
exports.initWebrtcConnaction = redux_act_1.createAction('success rtc connection');
exports.webrtcMessageReceived = redux_act_1.createAction('message received');
exports.setLastTransaction = redux_act_1.createAction('set last transaction');
exports.setPayData = redux_act_1.createAction('set pay data');
//# sourceMappingURL=index.js.map
});
___scope___.file("client/reducers/webrtc.js", function(exports, require, module, __filename, __dirname){

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const redux_act_1 = require("redux-act");
const webrtc_1 = tslib_1.__importDefault(require("../services/webrtc"));
const actions_1 = require("../actions");
const webrtcDefaultState = {
    error: '',
    lastTransaction: {},
    webrtc: webrtc_1.default,
    isSending: false
};
const webrtcReducer = redux_act_1.createReducer({}, webrtcDefaultState);
webrtcReducer.on(actions_1.setLastTransaction, (state, payload) => {
    const isErr = payload instanceof Error;
    return {
        ...state,
        error: isErr ? payload.message : null,
        lastTransaction: isErr ? null : payload,
    };
});
webrtcReducer.on(actions_1.startSendingTx, (_, payload) => {
    return { ..._,
        isSending: payload,
    };
});
exports.default = webrtcReducer;
//# sourceMappingURL=webrtc.js.map
});
___scope___.file("client/services/webrtc.js", function(exports, require, module, __filename, __dirname){

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const events_1 = require("events");
class RTCHelper extends events_1.EventEmitter {
    constructor(name) {
        super();
        this.name = name;
        this.rpc = new RTCPeerConnection();
        this.candidates = [];
        this.connected = false;
        this.tag = '';
        this.onIceCandidate = (ev) => {
            console.log(`${this.tag}onIceCandidate: ${JSON.stringify(ev.candidate)}`);
            if (ev.candidate)
                this.candidates.push(ev.candidate);
            this.emit('ice', ev.candidate);
        };
        this.onDataChannel = (ev) => {
            console.log(`${this.tag}onDataChannel: ${JSON.stringify(ev.channel)}`);
            this.setChannel(ev.channel);
        };
        this.onDataChannelOpen = (ev) => {
            console.log(`${this.tag}onDataChannelOpen: ${ev.type}`);
            this.connected = true;
            this.emit('connected');
        };
        this.onMessage = (ev) => {
            console.log(`${this.tag}${ev.type}: ${ev.data}`);
        };
        if (name)
            this.tag = `[${name}] `;
        this.rpc.onicecandidate = this.onIceCandidate;
        this.rpc.ondatachannel = this.onDataChannel;
    }
    setChannel(c) {
        this.dataChannel = c;
        this.dataChannel.onopen = this.onDataChannelOpen;
        this.dataChannel.onmessage = this.onMessage;
    }
    async waitConnection() {
        if (this.connected)
            return Promise.resolve();
        return new Promise((res, rej) => this.on('connected', () => res()));
    }
    async createOffer() {
        console.log(`${this.tag}createOffer`);
        this.setChannel(this.rpc.createDataChannel('chat'));
        this.offer = await this.rpc.createOffer();
        await this.rpc.setLocalDescription(this.offer);
        return this.offer;
    }
    async pushOffer(offer) {
        console.log(`${this.tag}pushOffer: ${JSON.stringify(offer)}`);
        if (this.offer)
            throw new Error('can\'t push offer to already inited rtc connection!');
        await this.rpc.setRemoteDescription(offer);
        const answer = await this.rpc.createAnswer();
        await this.rpc.setLocalDescription(answer);
        return answer;
    }
    async pushAnswer(answer) {
        console.log(`${this.tag}pushAnswer: ${JSON.stringify(answer)}`);
        await this.rpc.setRemoteDescription(answer);
    }
    async pushIceCandidate(candidate) {
        await this.rpc.addIceCandidate(candidate);
    }
}
exports.RTCHelper = RTCHelper;
const WebrtcService = (function () {
    var _webrtc = new RTCHelper();
    return _webrtc;
}());
exports.default = WebrtcService;
//# sourceMappingURL=webrtc.js.map
});
___scope___.file("client/sagas/index.js", function(exports, require, module, __filename, __dirname){

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const fuse_react_1 = require("fuse-react");
const redux_saga_1 = require("redux-saga");
const effects_1 = require("redux-saga/lib/effects.js");
const json_1 = require("../helpers/json");
const ethHelper_1 = require("../services/ethHelper");
const actions_1 = require("../actions");
const constants_1 = require("../constants");
function* createEventChannel(rtc) {
    return redux_saga_1.eventChannel((emit) => {
        rtc.dataChannel.onmessage = ((message) => {
            return emit(message.data);
        });
        return () => {
            rtc.close();
        };
    });
}
function* initializeWebrtcChannel() {
    const { webrtc } = yield effects_1.select((state) => state.webrtc);
    const channel = yield effects_1.call(createEventChannel, webrtc);
    while (true) {
        const message = yield effects_1.take(channel);
        console.log(message);
        yield effects_1.put(actions_1.webrtcMessageReceived(message));
    }
}
function* setWallet(wallet) {
    const wallets = yield wallet.map((item) => {
        return ethHelper_1.getNonce(item.address).then((resolve) => {
            return { ...item, nonce: resolve };
        });
    });
    yield effects_1.put(actions_1.addWallets(wallets));
    if (wallets.length === 1) {
        const payData = yield effects_1.select((state) => state.wallet.payData);
        fuse_react_1.navigate(`/${Object.keys(payData).length ? 'txCreation' : 'wallet'}/${wallet[0].blockchain}/${wallet[0].address}`);
        return;
    }
    fuse_react_1.navigate('/wallets');
}
function* webrtcListener(action) {
    const data = json_1.parseMessage(action.payload);
    switch (data.id) {
        case constants_1.RTCCommands.getWalletList:
            yield setWallet(data.result);
            break;
        case constants_1.RTCCommands.signTransferTx:
            yield effects_1.put(actions_1.scanTransaction(data.result));
            break;
        default:
            break;
    }
}
function* scanTx(action) {
    if (action.payload instanceof Error)
        return;
    try {
        yield effects_1.put(actions_1.startSendingTx(true));
        const transactionHash = yield ethHelper_1.sendTx(action.payload);
        yield effects_1.put(actions_1.setLastTransaction(transactionHash));
    }
    catch (error) {
        yield effects_1.put(actions_1.setLastTransaction(error));
    }
    yield effects_1.put(actions_1.startSendingTx(false));
    yield effects_1.put(actions_1.setPayData({}));
    fuse_react_1.navigate(`/tx`);
}
function* complementWallets(action) {
    // TODO: make notification about not valid qrcode
    if (!action.payload.length)
        return;
    yield setWallet(action.payload);
}
function* rootSaga() {
    yield effects_1.all([
        effects_1.takeEvery(actions_1.scanTransaction, scanTx),
        effects_1.takeEvery(actions_1.scanWallets, complementWallets),
        effects_1.takeEvery(actions_1.initWebrtcConnaction, initializeWebrtcChannel),
        effects_1.takeEvery(actions_1.webrtcMessageReceived, webrtcListener),
    ]);
}
exports.default = rootSaga;
//# sourceMappingURL=index.js.map
});
___scope___.file("client/helpers/json.js", function(exports, require, module, __filename, __dirname){

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.parseJsonString = (str) => {
    try {
        return JSON.parse(str);
    }
    catch (err) {
        return Error(err);
    }
};
exports.parseMessage = (data) => {
    const result = data.replace(/^([^|]*)\|(.*\D.*)\|(.*)$/, '$1|"$2"|$3').replace(/^([^|]+)\|([^|]*)\|(.*)$/, '{"method":"$1","id":$2,"params":$3}').replace(/^\|([^|]*)\|(.*)$/, '{"id":$1,"result":$2}');
    return exports.parseJsonString(result);
};
//# sourceMappingURL=json.js.map
});
___scope___.file("client/services/ethHelper.js", function(exports, require, module, __filename, __dirname){

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const Web3 = require("web3");
const api_1 = require("../services/api");
const axios_1 = tslib_1.__importDefault(require("axios"));
const web3 = new Web3();
const unsign = require('@warren-bank/ethereumjs-tx-unsign');
web3.setProvider(new Web3.providers.WebsocketProvider('wss://rinkeby.infura.io/ws'));
async function getNonce(address) {
    return await api_1.jsonRequest(`account/${address}`, 'nonce');
}
exports.getNonce = getNonce;
async function sendTx(tx) {
    const txData = await unsign(tx).txData;
    return axios_1.default.post('http://18.221.128.6:8080/rawtx', tx)
        .then(response => ({ transactionHash: response.data.txHash, ...txData }))
        .catch(error => new Error(error.response.data));
}
exports.sendTx = sendTx;
//# sourceMappingURL=ethHelper.js.map
});
___scope___.file("client/services/api.js", function(exports, require, module, __filename, __dirname){

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.jsonRequest = (path, nested) => {
    return fetch(`http://18.221.128.6:8080/${path}`)
        .then(resolve => resolve.json())
        .then(resolve => nested ? resolve[nested] : resolve);
};
// export const fetchAvaliableCurrencies = jsonRequest('blockchains', 'supported')
// export const fetchWallet = jsonRequest('addresses')
//# sourceMappingURL=api.js.map
});
___scope___.file("client/constants.js", function(exports, require, module, __filename, __dirname){

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
//TODO: make prod and dev url 
exports.handshakeServerUrl = "ws://localhost:3077";
var RTCCommands;
(function (RTCCommands) {
    RTCCommands[RTCCommands["webrtcLogin"] = 1] = "webrtcLogin";
    RTCCommands[RTCCommands["getWalletList"] = 2] = "getWalletList";
    RTCCommands[RTCCommands["signTransferTx"] = 3] = "signTransferTx";
})(RTCCommands = exports.RTCCommands || (exports.RTCCommands = {}));
//# sourceMappingURL=constants.js.map
});
___scope___.file("client/components/Home.jsx", function(exports, require, module, __filename, __dirname){

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const react_1 = tslib_1.__importDefault(require("react"));
const fuse_react_1 = require("fuse-react");
const react_redux_1 = require("react-redux");
const SupportedCurrenciesList_1 = tslib_1.__importDefault(require("./SupportedCurrenciesList"));
const layout_1 = require("./shared/layout");
const typography_1 = require("./shared/typography");
const buttons_1 = require("./shared/buttons");
const inputs_1 = require("./shared/inputs");
const Home = ({ wallets }) => (react_1.default.createElement(react_1.default.Fragment, null,
    react_1.default.createElement(layout_1.Header, { to: '/' }),
    react_1.default.createElement(layout_1.Container, null,
        react_1.default.createElement(layout_1.Centered, null,
            react_1.default.createElement(fuse_react_1.Link, { to: '/login' },
                react_1.default.createElement(buttons_1.ButtonBase, null, "Login using QR code")),
            react_1.default.createElement(layout_1.Row, null,
                react_1.default.createElement(typography_1.Separator, null, "or")),
            react_1.default.createElement(layout_1.Row, null,
                react_1.default.createElement(inputs_1.TextInput, { type: 'text', placeholder: 'Type your wallet address here' }),
                react_1.default.createElement(SupportedCurrenciesList_1.default, { supported: ['ftm', 'eth'] })),
            react_1.default.createElement(layout_1.Row, null,
                react_1.default.createElement(buttons_1.ButtonSecondary, null, "Login with address")),
            react_1.default.createElement(layout_1.Row, null,
                react_1.default.createElement(typography_1.JustSeparator, null)),
            react_1.default.createElement(fuse_react_1.Link, { to: '/webrtc' },
                react_1.default.createElement(buttons_1.ButtonWarning, null, "Webrtc login"))))));
const withConnect = react_redux_1.connect(({ wallet: { wallets } }) => ({ wallets }));
exports.default = withConnect(Home);
//# sourceMappingURL=Home.js.map
});
___scope___.file("client/components/SupportedCurrenciesList.jsx", function(exports, require, module, __filename, __dirname){

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const react_1 = tslib_1.__importDefault(require("react"));
const inputs_1 = require("./shared/inputs");
exports.default = ({ supported }) => (react_1.default.createElement(inputs_1.Select, { flipToRight: true }, ((supported) || []).map((v) => (react_1.default.createElement("option", { value: v, key: v }, v)))));
//# sourceMappingURL=SupportedCurrenciesList.js.map
});
___scope___.file("client/components/shared/inputs.jsx", function(exports, require, module, __filename, __dirname){

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const react_emotion_1 = tslib_1.__importDefault(require("react-emotion"));
exports.TextInput = react_emotion_1.default('input')({
    '::placeholder': {
        color: '#93a7b3',
    },
    background: '#eff9f9',
    border: '1px solid #457b9d',
    boxSizing: 'border-box',
    color: '#457b9d',
    fontSize: '.9rem',
    outline: 'none',
    padding: '1rem',
    width: '100%',
});
exports.Select = react_emotion_1.default('select')(({ flipToRight }) => ({
    ':after': {
        border: 'solid white',
        borderWidth: '0 3px 3px 0',
        content: '""',
        position: 'absolute',
    },
    appearance: 'none',
    background: '#457b9d',
    border: 0,
    borderRadius: flipToRight ? '0 .2rem .2rem 0' : '.2rem',
    color: '#f1faee',
    margin: flipToRight ? '0 0 0 -.2rem' : 0,
    padding: '0 1rem',
    boxSizing: 'border-box',
    position: 'relative',
}));
//# sourceMappingURL=inputs.js.map
});
___scope___.file("client/components/shared/layout.jsx", function(exports, require, module, __filename, __dirname){

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const react_emotion_1 = tslib_1.__importDefault(require("react-emotion"));
const fuse_react_1 = require("fuse-react");
const logo_png_1 = tslib_1.__importDefault(require("../../public/logo.png"));
exports.Container = react_emotion_1.default('main')({
    alignItems: 'center',
    background: '#f1faee',
    display: 'flex',
    justifyContent: 'center',
    minHeight: 'calc(100vh - 8rem - 4vh)',
    width: '100vw',
});
exports.Centered = react_emotion_1.default('div')({
    alignItems: 'center',
    justifyContent: 'center',
    textAlign: 'center',
});
exports.Row = react_emotion_1.default('div')({
    display: 'flex',
    justifyContent: 'space-around',
    margin: '1rem 0',
    width: '100%',
});
exports.Column = react_emotion_1.default('div')({
    display: 'flex',
    flexFlow: 'column nowrap',
    width: '100%',
});
exports.Header = react_emotion_1.default(fuse_react_1.Link)({
    ':after': {
        background: `url(${logo_png_1.default}) center center no-repeat`,
        content: '""',
        display: 'block',
        height: '8rem',
        width: '100vw',
    },
    background: 'linear-gradient(60deg, #abcedb 0%, #abcedb 49%, #c5dee7 49%, #c5dee7 54%, #d7e9ef 54%, #d7e9ef 100%)',
    display: 'block',
    padding: '2vh 0',
});
//# sourceMappingURL=layout.js.map
});
___scope___.file("client/public/logo.png", function(exports, require, module, __filename, __dirname){

module.exports = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAASYAAACACAYAAACm9sSdAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyRpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuMy1jMDExIDY2LjE0NTY2MSwgMjAxMi8wMi8wNi0xNDo1NjoyNyAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENTNiAoTWFjaW50b3NoKSIgeG1wTU06SW5zdGFuY2VJRD0ieG1wLmlpZDpBNEU5OUY3M0M4REExMUU4OEFGM0U4MzAwMUUyN0JGNyIgeG1wTU06RG9jdW1lbnRJRD0ieG1wLmRpZDpBNEU5OUY3NEM4REExMUU4OEFGM0U4MzAwMUUyN0JGNyI+IDx4bXBNTTpEZXJpdmVkRnJvbSBzdFJlZjppbnN0YW5jZUlEPSJ4bXAuaWlkOkE0RTk5RjcxQzhEQTExRTg4QUYzRTgzMDAxRTI3QkY3IiBzdFJlZjpkb2N1bWVudElEPSJ4bXAuZGlkOkE0RTk5RjcyQzhEQTExRTg4QUYzRTgzMDAxRTI3QkY3Ii8+IDwvcmRmOkRlc2NyaXB0aW9uPiA8L3JkZjpSREY+IDwveDp4bXBtZXRhPiA8P3hwYWNrZXQgZW5kPSJyIj8+ja5t7QAAIjxJREFUeNrsXQl8HVW5/845c2+SkrakLUgLWAyKClqBVBZxeY+XqvgW/SGpPrcq8lp9z93fM3UBkUUS9bmivlZReYhio/4ERcTmuTxc2tKgQEVECLSWpi1Jb/bkLnPO+84suXMnMzd3v5Pk+/9+JzN3Zu7k3LP85/99851zmFIKCPXDKz76VfzLMJVWD9I0z2ha1vL4ypNPu9iUmQuxOn/JAR7FU0/mXIeJ+/al85n7rmOYFcmFtWVMWlsXitnn3a3/uLvvhz7HYzEYPvw3GBs8AlyIvL/r55/8d2ocixicimDeYg2mjzClbjNTyXVIB6uwOj/Ogf1CMXY3nvs2Jt27L8J0HBUXYT7BoCKYd4hjeg2m92G6kDEGyeRkSzqV6jcaGkdUJrMc1clZqFEwwRsVU8P4+XHcvxfTbzD9DtNBTEkqSgIRE6ESWI/p/Zg2unVnMqYYZ09LmenfGdA4hIeWZ80pbVux43H/HPx4Dii1GQ8MY7of0+8donoEkyauDBUvgYiJUAxWYLoC0zsxnZbjK2KAmglWxYz4U/hxyvX12P4ehgSlcJ9ZRIUmnj5+PG5ehpz1MmafRlJS+xyiug/Tg5gGqMgJREyEMDRheiWm92B6Gdhe8hmntQ0GZib9tEx62ow3NB62zTjPWY8n2nVa27DUlP7UioTVitt/sW6tlHacvwPTL53/p/1T41QVBCImgsZzMX0A0yZMMX2A+0iJO/QiuTg1k0xNQZMcdM24fG/ObHJi/muUBLmTA/+SZGwvfj4FbF/Uv2E6G9NPMf0J7Dd+01Q9BCKmxYVlmF7vqJZzvCek70LblENmMdXy5MR4I2tZdViTlQmzX98zpSxTzlVM7nmUSJIr7RhX2xjw70nGV+C568H2Z71FKXYPfusaPPYWvH4/HvsLpl2YNHntwXSEqoxAxLRwoSnjEiSWDyLhXJz/UssM8yqotRxUM1LP30zLj6RmyMerlrzmnE1aqh+k+hzeYztww8Bjb8ZzH1BKnaH5Do9/HC96M+5/EM9tw3NrcbsWty/HrX6rdxhTH9hv+rQj/TFMg1SVBCKm+Q/NL+vwz+VIBG/G/eO9JyDAfJOQG8GolFzWtHTZciNmDGSmk47ryB/w6PU1qSNIPt9BBfUlxfhhxvhrtclmMvh7TWqM2QGfuH0dfr4D7/N1vMdluN3gIbsGl6hw/1JMKUwPge1A/y0m7VD/M6YxqmICEdP8QzOmK5Bs3oKks9QbAS7DxVLOeSSQhvT09BIlYQD3JZIFB5jtY9IqBwnpNiSjL6G66gPGL0Iz7UYkn5fjtYaYsfNmvhRDBbYVP/0C968RoNrAfkMY5MeK4772RZ2Nny/XJh5+7sftbidp00+bgmmqckIhT2tCHaFSU1Mqk/yIKTPnYHo9csLnkTh2YTripRWep/KQjJZOTU6syqSTgxzY8CwbUSlNSD0S5CtRJb0V7z0iOf8CHv8xnn6VfkCpmZACO0lgLumcC1aEuWWqfYU5Y5i8pJQbojCj0p4GOgBUWYGgOgr9YcznhzkwqnQCKabIE5OUrVJmXs4Z65MS7gbBvwc81owE8BwljHNAGBcxxZ6PpPIsrajkTOCkI2w0KXBxXDo5sRpNMm06TUmPvML77EUy+qwEsYMxpcniE3j8bUKpUy0Swj/uPS0DTtm+K3Ac5rZZyDYrpnYg2/wXnt6AV53vxETl+K2ypKR8piP+qlj80Uw6/ePJ8eE1wNlSJKm/UO0TiJiiCsYzHLh+JX8d4/BHU8r7hZncpUDea5rG1xjnN2EnXgax+JlgjXvj5+GXzkViOJmDalC2L4gpqdZOjI9ONy9rGVWZ9Ml47d+EghtNxrehQkojVbyXK3g7ktaZjHlCBnDL3Td2Fg8pi/b0vnTIx3KsM3Y1fvfVeFUXEuUPNH95HeosZwwy85ijCuKx+DEzleo80v+nP6TSye/G4o3L8R+8msw6AhFTdPEY0sCXsDNvx/2XcsZfih3+CqSDI9in/8KkuQs79x4uMw9IJfcybqSkET8JSWe94vx8VEsvQutqHRLDqdwaoaLfkql7kIw+ZQI/iPf6Z1Q+H+QMLtDkwx3Wsc017Yxi4M5uwGac5K6Tyr5Oayn8H6/ECzbj/o0C1C34/za5viYVYp1psjK4gHQq9eWj+//6w0wy+e6GeHwj5ochyaHysmKjCAQipojKph5tXmmfjEMMTbg9jTN1GhLUK5AkJkA7jhnsQ9PvPpaeuhcV0h7JxE+4MBqQNJ4rOdfkkZRSfgjJaxCV2IVMyi/jwYtNBjGhsgqGOSEFnLnTrTDLp8Q9hCJt35XzHQChI8wBOpHI7sL7XYum4Etw2ypCpjqxzTuGlmjsjmOHDnRPTYy8pKlhydWaGx1T8wpuR5dPUf0T/CDndzSgHda3ZudlmiVB9LAQNOX4RiScLtzuUFz8FEnmmyqT3ARmirFU8uHRgf2p6ZHBgdRYIi3NzEl4PoWK6jHBxTgIPuObkuCOm1OWyWa6od+e+Cfuqig8JCzlZO3raPAP4/Yx/PwpDllScs25rFmnkJSMfcmJsQ8kR4ZORHPus3jdCs8cUJeAnaw8+BOBFBMhGtgB1kBddnaWnLwElUNYK3F/JX5sU0y8AffHlTR7pcxslhlzOUDmhOmhQz/nIr5DHLf0eK7kOcyIv9AwGl6E33kuqq5TwR6HZ8dFWQ5vx2xzzDc+48BWMyTm/Ps34s7PJPBvMiVfi7JqAwsgEmbERjJTkx9+cl/f/thxzd/igq/3nkeCasTNuzHdzWxFSCAQMUXQnNOzA3wDieCLwcrJv+/6h6z5mVYgOfwdcsdy4OJF2O0/zSH2IJpyezNjiT3MTN2vRMMvMw2NBufiFLak+SzOxIvRVDsXCeL5qIhWa0e3kvZLOOb8b01G2mnFHYXl5AkJRX1MKfZrNO+24iErtskbMyUE3lGqrqGBAz9RcfFeZojXacLzj/VDvEQrJ/za9xVFERCImCKL72H31r6mc7IqKUtEQeRkT22ifUSqyWBiDef8KeSAEznIS4DzS5CARtGm+hvKoofM5NQuU6rf89TUPUhad3FhMN58/NoM4xcawC7A685HgjidSWiRSEnuFLzM89bO/s/sBXj8PUhaV6LS+joe+lBOsKUwfjB+5MlPpcZHXtHQsGQr3sdgEGilCjy3BQ/dBaSaCERMUUKOHXQUe+7/4LEX6DiCkGs8b9McokDGEIwvVWZKm2j7sLMP4enVzullejZLxjEBvxQEjKAJ9jgqmPsy6dQeNjx4nzLNO9NG7NZYU/MS/F/r0Ow7lzc06ajw5zGpTsN7LdOsxJy82EQI78QM3o5E9GkkpHZM51pMYxh/HD60/wNPPf7I2qaWFZ9CVXYSl9Imtpn85jjL9XQur7PVIoFAxBQVEy7I16SHdDx/9jXZwbtZ+eE6rK1X+isZF8NgSu1MX+0lNXuyOCVss4uvAMHb8NuXW+PmOH8YFdQDqcmR3czM9Ckj/hWRbPgqksoJYsnSZ+M9L8I7nM8Z04R5Kt7JwOtX4r2+YDK+QSh5A+73oEIbZAb/z+TE2OFYc/N3UL2tkzNBmpAzs4EL/C0xyeBy3P5Q2i8BCAQipgjiENhDOLpnqyUWSm6cWVHkJydTU+OG0ZBQ9ru3rOYK8lCD0sPj1qBptgaEuFgomFLCGOAKHlbJ6d0myF1mMnk/XrdLcSMdb162Gr/wQiX4BcKIvRhp5QWmMt9uCuPGmFS3oC569MjD+3rT6eTVDfGGy2SwX8mn/iy8CNM/gvVmkkAgYqo7gjst+zZ26LeCNVmcVy2pQAWVNenkMzKTk9MNS+OHMr43eo5igtlLRXl9VdabulbctqJN9irMwxgqqgNSwf0gM32ZkaF7U9LchSR2R6yxOa6EODsWb2xOTYwZDctWvndy+ClzcnzkDUZj08eCflfgRHe2oNL+LD3/1J1AqolAxFR/8OD15LRq2obp89n14FiAbyqXfNCcO9HMmBncDntJLGBK3QCym63M8HtL8XtnMcbPwiNvwEODgvNHUQk9mJkc262UuYfFmx5CE1ANHRtMTCengDc2juP1t2Oez8S8r5VWWIIdDyW9Q1iYQ1Js5j+jAoPX4Pe+Ra2CQMQUXfwIe+07sKM+x1VWPIdUmM/PZM1keaKUab1KykFm+5xcwoLZC2qqGZJwz2cJzFVQfoXFVinGVzGmLgAuNjEwhjFfv5US3pU4emjIiMWeIZj6tWT8DjMWP54D1w7xC7gwLkJSeg6qr5NBj+9joct76sUW9IwHQ1T9REyEuiI0gGe/7XNR12Yd3v6wAZajeEwOp3LBmxlnA8p0L2Qzr/tzlRHzkBa4plxA3pTHVFROSIBFWnHOjRNx977J0cQxA1gn/u+3glR/QkW1h6eT94KUD+HVv5CxOGaftSoROws4ezFTmrCkntN8tU+tnYf/4nWYpa9QuyBiIkQWCs0apgfLPjPkvJ/YjEw6fQpwfsSazURHEcyQidcBrkKIh/mmLJn95s8mM/szEtF3xo4NXmdOTXewuKFjmY5DG+0ZyEL/hN8bUZzvByUfUun0LlR7e5SZ2Yl3uB3/TSPEGk5BRXUh6PGBnJ0ngT0T87oc7/1BvKYX7PXuCERMhAjiIBLBbdjJP5aflJwwAs6WmBNjJ6bGxw7zhqZJUJmlfjKZTUhBc4Qrn4nHPN93pkrhYt/o0OGtowMHnxdrXnY9HjsuO8jN+h6alGwdML4Ov96BZxJ47Ak0L/fidbshOdlnAtwhFNyi4o1NOmgTrzsPROwf8Pw6IiYiJkJ9VdFcF9yM1+jxac/wKhjvbAAuOOMGZNKnmKnUPtHYOKx8U/WGkZprnmXJyFVXXsLKkhmacCOp5PSHJoYGE0bDkm2MsWcxNcsfBZ64K4H7q7SPClltPe6/HfePoDn3Z1OvCpxO7mZK6tWBb0QldRMY8UZqF0RMhGhDr+Om52q6wetbsl+9e+dTsoH222qQ5igeT5uh/izl8zvlKiIvAbpqySUtTBnTNG+YHB66iwvxaUyXgFIQMitCmLrT6+SdgttT9CBg3J9UjB/D3D+CxHpNxsz8mqp9cYOmPZkfQHMO/pLt2MpXecpRUQjBn47bNKYns76j2eabYkFqbfabPpewHFJCtcRvGxs89LmpkeHL8MO7Zqs+FmIy5j2/BKzZBvguNP3+CjQvOBETFcG8wBNghQ+AT3nkdnZ7Yjf29OnJsRQzzYR73BsCwJQKIAjlLCWuAsnJ3RdC/Fkmkx/NTE6eJhrin+SMNWbvxQJNUxma5xwl9b+4fymmj4Idw0UgYiLME3wTO+5ALqkwj4JiTuAiXy4zqeXIM/u9vqOsP4nNTOTmvVe4c1yTlkRSMsbMVGrrsUOPH8VbfhFJ6Vn5gjOzqi43j+CYoM61+HvU1bizEdM9VMUEIqbIgBWYtCnHbgs2u7yObNbCuVgCptwvlDsnN5shKKaUJyTA76xmM/4nT/Q3gGFAJpP5zPDAgZ9LU36ScfGKYNPPP2wmS0gSstP0cuuz6sVzb8Fzn8BDx6gdEIiY5i++ZZs6KsSHY3X+VUzwFYzzQ9I3iFcx/xs25TOrVI7/yXZ8W+tD3TmZGLwhnU6+yYjF/yPYLwUhZJfb0HA7guc/jeSEpGTFKxEIREzzHA9g+m6wA3mGZJpkxlxpMkigWSf9pJFrxkGICZY174yY8Uh6Yvz9ydHhM7kQV+Gp+GwTLpyQfOSpQwL0lC4fwjRA1UkgYlpQqsn1NfmJwTKThDk9fbo5PXkUOB9WCgLUEfO8qct+9i4koCFELJmcGL9q5MknjiDDfVZwcWquwoIAgvL6kJTr/J7AfR3y8FpMP6QqJBAxLTzsw47+g6C4IYsQ7JV5n25KOcJBTTOWDRfIxi55Y5i8pl52vBwTApKpqS8MHuj/Hn683ojFLs51YjMIHrMHntk1rUUOtMrT0wXrAbqPUfURiJgWLr6G6UjuISfYUpMP5ydKqcYV40fDAx9tkgk17YT42fRo4hrG2RW8Ib5Zgd9h7ldMs2KWtEr6Km47cNsD+eeLIxCImBYAHF8T5Jhn9gY1ijSflhkfzZhKm3zcEyQJ4H+TZyunXB+REOJAcjjROTU2+mwRb7gGz8W9ZOZeL0PJSf0J/2gn+XuBxrwRiJjmI1SJSY+hyzqQped+zGCrRTzOhVJDs4MlwRdkmVU/1uKWwkiCVFunE0NPMKm+wBlbnUt+LFeh5R43MR+34P5GJ39pql8CEdOiU03aRPIOT5lZeKAlnUo2K1MdZYxD7ls05pk4zjeY11qtl315aODA7Zl08gZhiBcDBJl6ftPN2j6K2yvA9iU9RNVDIGJanNDLU+plxYdmkwxbk0mn12TA9IyX8/uHcl/pa/XDuPjd8JEDn0yOj16uYsYVwFxzzb8icM425TjjO8COs6L14QhETIsce0Avm5Rjllm+JO0TWsWVGsid61t5SCr3uBLiYHJq4p3TkxNniIbGq1BpxcMbyIzpdhC3epnvt2L6I1UHgYiJ4Mqm/5bABrOEAfZc33rBSykHkWCmvPMsZWcMUDNj6JgQaZlJXzVxdOAwY+xzgvMT9Pg4P+F5lJb2Hd2O+5eBPSXLONUEgYhpQYGVkawKfADTT70KSCAJmdI8QSk086ywAfAppOzAXs45mOn0TcOH9t+aTie7kJTO95KcnO081+bhlWDHJu2m+iMQMRGCoBnjFkxjM4RiDdqVramp8VHO2Vj2sqwCcgf3ChG7Z2o08QlzemqzMGJv8N+a5xAo/B+mN4G9GGeCip5AxETIBz1lyA9y5vFm7CQlM2jVyWFvrJJ35gDB+OD46LGPJMdGnsvjjddwxhrAMwuAR5lpU/Hj9hS/8CsqbgIRE6EQJMGar4mNukqHKbZWcS6RoA5rMnIjvO0pUKSet9tEM+3KyeGn9psZ8zOMs5bcYSUzKmu3Y7ZdA5azm0AgYiIUjt8gkdzpcVQ3KSkbLD9TjimnwIpt4uym0cSR7yIpXcdjsXM95pp7nZ4BU5tseuDtT6h4CURMhFIgJbCvI5lMWrTCYJmZSi5VytyPJDTzVs4mJXHf6FMD1yeHh9+Gn9/EZznH1b34582YPgKeucMJBCImQimVqX1NvbaPiWtiOimdyTxprZ7kjoMzeGJqYux90yMjZwguPsr0azmPSpIAn8PvX4of7gQaeEuoE2j5poUFHV90Eya9JFIT8soapuQQ2OsraQ7KpKaT144fGzjEOLsDBF9lxytZkukPuL0BWer7UMBidwQCKSZCMbhb+5qsGEturOZcHMNKnmLWXEv81omxxM3SNK/jgp1pX86m8M838Dv/CnYUOZESgYiJUHEkkWy+KhhXZiZzBkokHWApmBF7MDk5cVVycvxyJKvXOyrpQbCnut0CM+vWEQgRNuXYlTcv5nIJUg3aPKr45Pntec71Nq0MzEf71NBc+bgHeec3uD0tNT0pG5qXD2TGx68aOXLwLHwSXcuFSIEd93Q11nQk50ta5O2v/AZ87SZSTITIQfuatimpTmCKN6eS05cfO3pwVEj5FS6Mp/Dce6StlGgSN8L8UkwFoBVTm7N1H/z6c4vnmj6why4knP1+R3XQcIbq4yciJi6bmhiG6fGEnlHyLhaLYfmrTtzfW86NUcmVXPeo9qjuCRUnJt3wNoM1j7PVEOeC95oOz36PJxGqgxE0iLarjBkHg20STPwKSUlPkztSIhlVpO7xPla9I0FR3RPKJibdKDudhtlSgf/b4TxFCdX1NNzNYqKFMfirVLAf7XazREKiup9nKMBH1xFlYVAIMekG2VWhRgk+1bTogR2/mt/RQUpDTioFVav7Un43oSy0OWZ3u8f8ZvOVmLY5jTMf+jw+hH7I9R+5cr7d54NwrydEF1T3Cwe6DnbOe1MOZWCL80PCfAm6YW13VE++Rua+1u72NNYOIOd3lEF1T4geMc1BSgmnoXWX+P/6yL8wb0mJ6p5QV8XUFdIwdaPaQo1rQYPqnhA9YkK1tDnEr6Ab5AaS4QsaVPeEyID7TLiuEJ8CNcyFb8JR3ROiR0wQ/lp4CzXMRWHCUd0TokVMjloKkvH67UtvhPKr/R862E87aB+D7CqOyvm80znfGrFybvXk+5gv3zsh3LdTK7UUlbpv95WNcsotqB1s87WBdt9vUgFpR5n52xFwz70l/gZ/u3gs4L6FhGwEYWdAGwuCypM669k3DY9/IQjdEenYHc6Papuj87tjt7qcTtVdZ2JtdRpX+xydsd35fTqvW6G2Tuao172fRDsL6DQJh1g3B7SjlhJVYAvkDqvyEngp7aIr5H7ejt7mUbTdEa2TqvRN7rm5H3PFqdSqY+90nlTFKgo3qGwbVD5yudAO/9gcpBSU570lPiXLaVhRqfv+OYhhZxFP8p4iibiUcgr6P/0h7djfLjqKJMUu5/e3RIOPqts3OZpxrSE3rrcJ1+Z00vYKEEStK3Sbk8r5fkeNGleU6r5/jjIppgP0htyvo4x2FKSWEkUQU2eZ7aI9IuRU9b7J89y8p84/PKwCdIPTTtkVkLte9noIXyG2rYYV2pnnqewGKW7wpe6ABr2tBnmNYt2HNeBSCGX7HCZSLQncHXfoJzFd9xs9bWFLCOEV05Y3+PrGhpDr8q0/313PvslDnFH9UL+3MS15fvgWp5CDKq7P8c+sD/HRtNWgs7dD8Gt3cPJ2urPt9SXvOW851EIxRanu85G9nzg3+hp/bxH+n2JJriOknHqKKOcu3wPKW+c9nraw3Wnn/vbgb8udC7lv8pAnQT19S2F250YozNHoxt70hTSwjirnHUIqrbuADu+qqVoRQ9TqPkxptHo69BanLRRCComQNlMJYirG6d0KuYOYN8DcjmxXXW/JQ9atC7Vv8jySrB5oD2kEW4s0LxJ5OnhXDTqQP+/FNOJeqN3blyjVfSGqrtiyDDNLW4sgpzAzrhRzN+F04mLeum7Po5w6F2rf5FC/+Jmwzg0hdngpjaC7zEZZbt77Ssx7rcIcolT3hRBMKa/mw5zghTpuO4q451zYWuL3uvOojJaF2DejtBhBJeNEvN9NlNEoi3mqt1U47zSR3uxOXU47COpohXTqjgrVTX+Z7aE7pM+0L8S+GSVi6qhCB02EfL+jBnlPlFlxvUDwKs/+MjsBlNAOgh44pdbr9jLLICy2rG0h9s0oEVOYI7a/Ao066AlQScdhaxUUD83yWDmSTuRRTbVQS5Wqz946EVPN+yav448tpHNXYmhGXxH/r5J575+nHT6KfqdKlGVPyG9tLZKYSlU+iSqVQ+tC7Js8j00ZBcVUzR/fNg/yXg+0RDBPlVIbxUSCB5lx9Z6Fs69OxFTzvhmmmNqpQyyKvEel7muFYsy5Sppx8x01b988j8RsB8JCx2Kr++0hiqO9ymYcoQRi6iViWrRYbHVfaCR4kBnXA9GcNG9BTuTH1bWb+kNsvUqtvFpOJ6mEH6itSPu2FNTrNW4l8h2Fuq8lCgkfiaoZVy9fZs37Js9T6C1Q23mB+mv84/urnPfWedxRa133tVaJ/QG/tyMPMRUzYLeaqNfb35r3Te6xnYMkYS0HCoZ17tYq/Pj+CldoXxXMoVoprijUfa2xPU99hZlxEIH67IgQMVW1b1rEhOZc2NgV/STZUaPG0lNEZRSKsCd/paOqw17jlkNOtfLzRKHuo0BMHXnKvRJO70pMqtZSJxOz5n2TF/DkrMU8Ri5T9lbY37G5iIZZbsWFqQ6ocN5rqZpqVfe1RpAT3B131l6A6VcqMZVDTh0hD8T+hdg3Z4jJUU1b8txkB1TfIRo2RUUpndQ/OZe3ofXVKO/tJT5Vam1GRaHua41C66uSiqSrxHLsLFPJ9VVAxdW0b+ZEfiM59UD4NAa6wvZCdSdaC1syqKvIAshnhmytUt7Dyq3Yuao3Q/XmjJqr4dWz7muNICXUGUDYlSSmthLIKaw9FDNbQb5peiPZN2cNSUFyyjcZV6tz070lyjh3StB8HW9rSEFuK9A0cucQbgu5d7Ver/aHkF6LU16dBVSYfxGDWs8wUO+6rzXm6tjViF3anKd9+ttDvsULtpRAxOUq85r1TaaUCrwDu/LmQhuRK78SAZ2+zSMZ2zyNWXfi0+eovG15ZOl23xPP6x/IZ7sWWplBhbKhQKLYC/lDFHo85eV2+DaYPemXe11XifkoB/Ws+3LKvljoPB3Lc359iQ8yFdJu2wLKz+9aaHGuy0f+W6H4CdrC6jThU0Pu27at9eyb+YjJlfDVWpdtrkrPVwClPBm3lNmwCu0cLQU+EefyCWzwPGFqTUz1rPtaEpP7tA97O7ShxHsG/YaNMPfCkNVoy17Fu7eI+mT17JtzzcfUA9nVGiotadsLyPAGKO+tgzvH8haoHRJldiS3Q9R7qEE9677Wvqaw3x+lduEqpVLbcrHT4LbUs2/yAm/Q7Wmk5fpo3ALqKbDRrC+hc3iXx6lHxK7bCLcUUXlunucipbYa/4561X09Ue40uHO1i2Lb83anDspdpMJddaWQ/91Wz745lymXTxa2Q26UbHvIk8j1P/RD+XEXHZBdsLDd92P7PKnchl9pc6LD42tp85ls3jwnApTFzgr4FyqJatd9FEy5csu40N+w2dMm2nzE6G0XlY5VcoMb2yE3gttbV9uLJJyK9s1SiYlAWAhwl7r2P9FPL9N8rTW5zs7AtZvmdcVwapuERYyuEHMnQUVDxEQg1ANB0dTlrmxDIGIiEMoy4YLUUjXeQBKImAiEgkhpZ8DxXlJLREwEQj3gDgdpCTDhtlDxRAcGFQFhEcAdzR42CFmbcLTAKBETgVAzQuqE/KPfu8mEI1OOQKglOuYgpe1QvWlwCERMBEIgwqKMXZ8S+ZWImAiEmiNoeSp3kDSZb0RMBELd0OtRTxuc1EfFEm2Q85uw0NENtR9mwqjYyyzAsEG8BAKBQIqJEK0nVnGzS6gApaDKUQ/zfXQ8oTyQj4lAIBAxEQgFoBMV214qBjLlCIQooYuKgBQTgUAgRAr0Vm6hV/CVN7sVvF5du6nPc1yvp6ZH2Z+Ox/uDjoP9il2PNdNDO9x5od0FBbwBisU4v92xa+79dHyRO4i2C4IXTux2FmIlkGIiLBC4wzLaPeTjXYCyI+B4v5Pc1WC9q5u4KwaXsmb9Tod83NVXeiG7/Li+r3+y+h7ILvxJWEQgH9PiICZ3lZZuPxk5+90+8urxqKMVkBuc6K7c0gnFDetwp7L1L3DorhDb5RzvcdUWqqSNVH2kmAgLE71+xeTs9zvn2lAptYQQEzik1OIQmHee7NYi8+GSod8k2x5AloTFrpg6f7SHSiEi6H7NeRW/J6qOBBKPJqB2x1TTRNPmEEK/QzQdzme9n9C+KE+AZZjfp1i466YdCznfUo8yp/YfzT5gQPAaWITao5rjq3od0mn3mXiuw1mTViJALbmk1AdZn1CizDYTtpBkPRcBoD4QsT5APqbFgR6XgBxlolWRZeIhIfU5iqk/gJhcpbQRyp96tt8x/7YDTWNLmAPcYSlK9U/VkwN2OEA/ZJdw7vGpKZeEZggrn/VTBjlqbCvUbEPSbK1RP6D2F7E+YFTDr0GIrGrqDFBFYcfdz1pN7XQIrBVmLxJZjAnnmpPHPGZhi0OWGz0k6Zqee5GcrFAFJMyeahQKtf/oKibC4oDb6XNUkRN0mQghpo2O6aXJY7Oz9U60Voyi0f9jvUNQXqd7m3M/r49pi3PMJS3CIsP/CzAAFgqgwezUbh8AAAAASUVORK5CYII="
});
___scope___.file("client/components/shared/typography.jsx", function(exports, require, module, __filename, __dirname){

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const react_emotion_1 = tslib_1.__importDefault(require("react-emotion"));
exports.H1 = react_emotion_1.default('h1')({
    color: '#59c9d3',
    fontSize: '2.6rem',
    fontWeight: 'normal',
    margin: '.25rem 0',
});
exports.H2 = react_emotion_1.default('h2')({
    color: '#469ea6',
    fontSize: '1.2em',
    fontWeight: 'normal',
    margin: '.5rem 0',
});
exports.JustSeparator = react_emotion_1.default('div')({
    background: '#babec1',
    height: 2,
    margin: '1rem 0',
    width: '100%',
});
exports.Separator = react_emotion_1.default('div')({
    ':after': {
        background: '#babec1',
        content: '""',
        display: 'block',
        height: 2,
        position: 'absolute',
        right: 0,
        top: '.8rem',
        width: '35%',
    },
    ':before': {
        background: '#babec1',
        content: '""',
        display: 'block',
        height: 2,
        left: 0,
        position: 'absolute',
        top: '.8rem',
        width: '35%',
    },
    color: '#1a3640',
    fontSize: '1.2rem',
    margin: '1rem 0',
    position: 'relative',
    width: '100%',
});
//# sourceMappingURL=typography.js.map
});
___scope___.file("client/components/shared/buttons.jsx", function(exports, require, module, __filename, __dirname){

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const react_emotion_1 = tslib_1.__importDefault(require("react-emotion"));
exports.Button = react_emotion_1.default('button')({
    borderRadius: '.2rem',
    cursor: 'pointer',
    fontSize: '1.34rem',
    outline: 'none',
    padding: '1rem 2rem',
    width: '100%',
});
exports.ButtonBase = react_emotion_1.default(exports.Button)({
    background: 'rgb(89, 201, 211)',
    borderBottom: '3px solid #52979d',
    color: '#f1faee',
});
exports.ButtonSecondary = react_emotion_1.default(exports.Button)({
    background: 'transparent',
    border: '1px solid #52979d',
    color: '#52979d',
    fontSize: '1rem',
});
exports.ButtonWarning = react_emotion_1.default(exports.ButtonSecondary)({
    borderColor: '#e63946',
    color: '#e63946',
});
//# sourceMappingURL=buttons.js.map
});
___scope___.file("client/components/Login.jsx", function(exports, require, module, __filename, __dirname){

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const react_1 = tslib_1.__importDefault(require("react"));
const react_qr_reader_1 = tslib_1.__importDefault(require("react-qr-reader"));
const react_redux_1 = require("react-redux");
const layout_1 = require("./shared/layout");
const typography_1 = require("./shared/typography");
const actions_1 = require("../actions");
const json_1 = require("../helpers/json");
const qrcode_react_1 = tslib_1.__importDefault(require("qrcode.react"));
const webrtc_1 = require("./../helpers/webrtc");
const Login = (props) => {
    return (react_1.default.createElement(react_1.default.Fragment, null,
        react_1.default.createElement(layout_1.Header, { to: '/' }),
        react_1.default.createElement(layout_1.Container, null,
            react_1.default.createElement(layout_1.Row, { style: { width: '100vw' } },
                react_1.default.createElement(layout_1.Column, { style: { width: '40%', margin: '0 5%' } },
                    react_1.default.createElement(layout_1.Centered, null,
                        react_1.default.createElement(typography_1.H2, null, "Scan QR Code")),
                    react_1.default.createElement(layout_1.Centered, { style: { display: 'flex' } },
                        react_1.default.createElement(qrcode_react_1.default, { value: webrtc_1.getWalletList(), renderAs: 'svg', style: { width: '40vh', height: '40vh' } }))),
                react_1.default.createElement(layout_1.Column, { style: { width: '40%', margin: '0 5%' } },
                    react_1.default.createElement(layout_1.Centered, null,
                        react_1.default.createElement(typography_1.H2, null, "Show qrcode")),
                    react_1.default.createElement(layout_1.Centered, { style: { display: 'flex' } },
                        react_1.default.createElement(react_qr_reader_1.default, { delay: 300, onScan: (result) => result && props.scanWallets(json_1.parseJsonString(result.substr(3))), onError: (error) => props.scanWallets(Error(error)), style: { width: '40vh' } })))))));
};
exports.default = react_redux_1.connect(null, { scanWallets: actions_1.scanWallets })(Login);
//# sourceMappingURL=Login.js.map
});
___scope___.file("client/helpers/webrtc.js", function(exports, require, module, __filename, __dirname){

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const web3_1 = tslib_1.__importDefault(require("web3"));
const constants_1 = require("../constants");
// TODO: add supported blockchain enum
exports.webrtcLogin = (sid) => {
    const params = { sid, url: constants_1.handshakeServerUrl };
    return `webrtcLogin|1|${JSON.stringify(params)}`;
};
exports.getWalletList = () => {
    const params = { blockchains: ['eth'] };
    return `getWalletList|2|${JSON.stringify(params)}`;
};
exports.signTransferTx = (value, wallet) => {
    const tx = {
        gasPrice: web3_1.default.utils.toWei(value.gasPrice, 'gwei'),
        nonce: wallet.nonce,
        to: value.to,
        value: web3_1.default.utils.toWei(value.amount),
    };
    return `signTransferTx|3|${JSON.stringify({ wallet, tx })}`;
};
exports.payToAddress = (value) => {
    // payToAddress|1|{"to": "0xadadad", "gasPrice": "12313", "value": "1231", "data": "0xadaa", "callback": ""}
    const tx = {
        gasPrice: web3_1.default.utils.toWei(value.gasPrice, 'gwei'),
        to: value.to,
        value: web3_1.default.utils.toWei(value.amount, 'ether'),
        data: value.data,
        callback: value.callback,
        blockchain: 'eth'
    };
    return `payToAddress|1|${JSON.stringify({ ...tx })}`;
};
//# sourceMappingURL=webrtc.js.map
});
___scope___.file("client/components/txCreation/index.jsx", function(exports, require, module, __filename, __dirname){

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const fetch_hoc_1 = tslib_1.__importDefault(require("fetch-hoc"));
const react_redux_1 = require("react-redux");
const recompact_1 = require("recompact");
const TxContainer_1 = tslib_1.__importDefault(require("./TxContainer"));
const withBlockchainData = recompact_1.compose(fetch_hoc_1.default(({ match: { params: { blockchain } } }) => `https://ethgasstation.info/json/ethgasAPI.json`), recompact_1.mapProps((props) => ({ ...props, blockChainData: props.data })));
const withBlockchainPrice = recompact_1.compose(fetch_hoc_1.default(({ match: { params: { blockchain } } }) => `https://api.coinmarketcap.com/v1/ticker/ethereum/`), recompact_1.mapProps((props) => ({ ...props, blockChainPrice: (props.data && props.data[0] && props.data[0].price_usd) || 0 })));
const withConnect = react_redux_1.connect((state) => ({ wallets: state.wallet.wallets, webrtc: state.webrtc.webrtc, isSending: state.webrtc.isSending, initialValues: state.wallet.payData }));
const wrap = recompact_1.compose(withBlockchainData, withBlockchainPrice, withConnect);
exports.default = wrap(TxContainer_1.default);
//# sourceMappingURL=index.js.map
});
___scope___.file("client/components/txCreation/TxContainer.jsx", function(exports, require, module, __filename, __dirname){

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const react_1 = tslib_1.__importDefault(require("react"));
const react_powerplug_1 = require("react-powerplug");
const fuse_react_1 = require("fuse-react");
const layout_1 = require("../shared/layout");
const TxSigned_1 = tslib_1.__importDefault(require("./TxSigned"));
const TxForm_1 = tslib_1.__importDefault(require("./TxForm"));
const TxHeader_1 = tslib_1.__importDefault(require("./TxHeader"));
const layout_2 = require("../shared/layout");
const TxContainer = ({ match, blockChainData, blockChainPrice, wallets, webrtc, isSending, initialValues }) => {
    const wallet = wallets.find((item) => item.address === match.params.address);
    return (react_1.default.createElement(react_1.default.Fragment, null,
        react_1.default.createElement(layout_1.Header, { to: '/' }),
        react_1.default.createElement(layout_2.Container, null,
            react_1.default.createElement(layout_2.Centered, { style: { maxWidth: '80vw' } },
                react_1.default.createElement(TxHeader_1.default, { params: match.params }, "New Tx"),
                react_1.default.createElement("br", null),
                react_1.default.createElement(react_powerplug_1.Value, { initial: { ...{ to: '', amount: '1', gasPrice: '1' }, ...initialValues } }, ({ set, value }) => (react_1.default.createElement(fuse_react_1.Switch, null,
                    react_1.default.createElement(fuse_react_1.Route, { path: '/txCreation/:blockchain/:address/sign', component: () => react_1.default.createElement(TxSigned_1.default, Object.assign({}, {
                            address: match.params.address,
                            blockChainData,
                            blockChainPrice,
                            value,
                            wallet,
                            isSending,
                        })) }),
                    react_1.default.createElement(fuse_react_1.Route, { exact: true, path: '/txCreation/:blockchain/:address', component: () => react_1.default.createElement(TxForm_1.default, Object.assign({}, {
                            address: match.params.address,
                            blockChainData,
                            blockChainPrice,
                            set,
                            value,
                            wallet,
                            webrtc,
                            isSending,
                        })) }))))))));
};
exports.default = TxContainer;
//# sourceMappingURL=TxContainer.js.map
});
___scope___.file("client/components/txCreation/TxSigned.jsx", function(exports, require, module, __filename, __dirname){

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const react_1 = tslib_1.__importDefault(require("react"));
const react_qr_reader_1 = tslib_1.__importDefault(require("react-qr-reader"));
const qrcode_react_1 = tslib_1.__importDefault(require("qrcode.react"));
const react_redux_1 = require("react-redux");
const actions_1 = require("../../actions");
const json_1 = require("../../helpers/json");
const layout_1 = require("../shared/layout");
const typography_1 = require("../shared/typography");
const webrtc_1 = require("../../helpers/webrtc");
class TxSigned extends react_1.default.Component {
    constructor() {
        super(...arguments);
        this.state = {
            isScaned: false,
        };
        this.onScan = (result) => {
            const { handleScan: handle } = this.props;
            this.setState({ isScaned: true });
            handle(json_1.parseJsonString(result.substr(3)));
        };
    }
    render() {
        const { value, wallet, isSending } = this.props;
        const { isScaned } = this.state;
        const sendData = webrtc_1.signTransferTx(value, wallet);
        if (isSending)
            return (react_1.default.createElement(layout_1.Row, { style: { minWidth: '80vw' } },
                react_1.default.createElement(typography_1.H2, null, "Sending...")));
        return (react_1.default.createElement(layout_1.Row, { style: { minWidth: '80vw' } },
            react_1.default.createElement(layout_1.Column, { style: { width: '45%', marginRight: '5%' } },
                react_1.default.createElement(typography_1.H2, null, "1. Scan this request"),
                react_1.default.createElement(qrcode_react_1.default, { value: sendData, renderAs: 'svg', style: { width: '100%', height: '100%' } })),
            react_1.default.createElement(layout_1.Column, { style: { width: '45%', marginLeft: '5%' } },
                react_1.default.createElement(typography_1.H2, null, "2. Show response here"),
                react_1.default.createElement(react_qr_reader_1.default, { delay: 300, onScan: (result) => result && !isScaned && this.onScan(result), onError: (error) => this.onScan(Error(error)), style: { width: '100%' } }))));
    }
}
exports.default = react_redux_1.connect(null, { handleScan: actions_1.scanTransaction })(TxSigned);
//# sourceMappingURL=TxSigned.js.map
});
___scope___.file("client/components/txCreation/TxForm.jsx", function(exports, require, module, __filename, __dirname){

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const react_1 = tslib_1.__importDefault(require("react"));
const recompact_1 = require("recompact");
const react_powerplug_1 = require("react-powerplug");
const fuse_react_1 = require("fuse-react");
const web3_1 = tslib_1.__importDefault(require("web3"));
const layout_1 = require("../shared/layout");
const typography_1 = require("../shared/typography");
const inputs_1 = require("../shared/inputs");
const buttons_1 = require("../shared/buttons");
const webrtc_1 = require("../../helpers/webrtc");
const TxForm = ({ address, blockChainPrice, blockChainData, value, set, webrtc, wallet, isSending }) => {
    if (isSending) {
        return (react_1.default.createElement(layout_1.Row, { style: { minWidth: '80vw' } },
            react_1.default.createElement(typography_1.H2, null, "Sending...")));
    }
    return (react_1.default.createElement(react_powerplug_1.Form, { initial: value }, ({ input, values }) => (react_1.default.createElement("form", { onSubmit: (e) => {
            e.preventDefault();
            if (webrtc.connected) {
                webrtc.dataChannel.send(webrtc_1.signTransferTx(values, wallet));
                return;
            }
            set(values);
            fuse_react_1.navigate(`/txCreation/ftm/${address}/sign`);
        } },
        react_1.default.createElement(layout_1.Column, null,
            react_1.default.createElement(inputs_1.TextInput, Object.assign({ placeholder: `To address` }, input('to').bind)),
            react_1.default.createElement(layout_1.Row, null,
                react_1.default.createElement(inputs_1.TextInput, Object.assign({ type: "number", min: "0", step: 1e-18.toFixed(20), placeholder: 'Amount' }, input('amount').bind)),
                react_1.default.createElement(layout_1.Centered, { style: { display: 'flex', marginLeft: '.5rem' } },
                    react_1.default.createElement("span", null,
                        "~",
                        (Number(values.amount) * Number(blockChainPrice)).toFixed(2),
                        "$"))),
            react_1.default.createElement(inputs_1.TextInput, Object.assign({ type: "text", placeholder: 'Data' }, input('data').bind)),
            react_1.default.createElement(layout_1.Row, null,
                react_1.default.createElement("span", null,
                    "Gas price ",
                    values.gasPrice,
                    " GWEI"),
                react_1.default.createElement(inputs_1.TextInput, Object.assign({ type: 'range' }, input('gasPrice').bind, { min: '1', max: '100' })),
                react_1.default.createElement("span", null,
                    " ",
                    (Number(web3_1.default.utils.fromWei(values.gasPrice, "gwei")) * 21000 * Number(blockChainPrice)).toFixed(3),
                    " USD"),
                react_1.default.createElement("span", { style: { minWidth: '5rem', marginLeft: '.5rem' } },
                    " ",
                    `< ${(Number(blockChainData.avgWait) * Number(values.gasPrice)).toFixed(2)} min`)),
            react_1.default.createElement(buttons_1.ButtonBase, { type: 'submit' }, "Sign"))))));
};
exports.default = recompact_1.defaultProps({ blockChainPrice: 0, blockChainData: { avgWait: 1 } })(TxForm);
//# sourceMappingURL=TxForm.js.map
});
___scope___.file("client/components/txCreation/TxHeader.jsx", function(exports, require, module, __filename, __dirname){

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const react_1 = tslib_1.__importStar(require("react"));
const typography_1 = require("../shared/typography");
exports.default = ({ params, children }) => (react_1.default.createElement(react_1.Fragment, null,
    react_1.default.createElement(typography_1.H1, null,
        children,
        " ",
        react_1.default.createElement("strong", null, params.blockchain.toUpperCase())),
    react_1.default.createElement(typography_1.H2, null, params.address)));
//# sourceMappingURL=TxHeader.js.map
});
___scope___.file("client/components/Wallet.jsx", function(exports, require, module, __filename, __dirname){

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const react_1 = tslib_1.__importStar(require("react"));
const react_emotion_1 = tslib_1.__importDefault(require("react-emotion"));
const fuse_react_1 = require("fuse-react");
const layout_1 = require("./shared/layout");
const buttons_1 = require("./shared/buttons");
const typography_1 = require("./shared/typography");
const table_1 = require("./shared/table");
const OverflowTd = react_emotion_1.default('td')({
    maxWidth: '20vw',
    overflow: 'hidden',
    textOverflow: 'ellipsis',
});
class Wallet extends react_1.Component {
    constructor() {
        super(...arguments);
        this.state = {
            address: null,
            blockchain: null,
            txs: [],
        };
        this.componentWillMount = () => {
            const { match: { params: { symbol, address } } } = this.props;
            fetch(`http://api-rinkeby.etherscan.io/api?module=account&action=txlist&address=${address}&startblock=0&endblock=99999999&sort=asc&apikey=YourApiKeyToken`)
                .then((res) => res.json())
                .then((json) => this.setState({ txs: json.result }));
        };
    }
    render() {
        const { match: { params: { symbol, address } } } = this.props;
        return (react_1.default.createElement(react_1.default.Fragment, null,
            react_1.default.createElement(layout_1.Header, { to: '/wallets' }),
            react_1.default.createElement(layout_1.Container, null,
                react_1.default.createElement(layout_1.Column, { style: { width: 'inherit' } },
                    react_1.default.createElement(layout_1.Row, null,
                        react_1.default.createElement(layout_1.Column, { style: { flexGrow: 1 } },
                            react_1.default.createElement(layout_1.Centered, null,
                                react_1.default.createElement(typography_1.H1, null, "FTM"),
                                react_1.default.createElement(typography_1.H2, null, address))),
                        react_1.default.createElement(layout_1.Row, { style: { flexBasis: '50%' } },
                            react_1.default.createElement(fuse_react_1.Link, { to: `/txCreation/${symbol}/${address}` },
                                react_1.default.createElement(buttons_1.ButtonBase, null, "Create New Tx")))),
                    react_1.default.createElement(table_1.Table, null,
                        react_1.default.createElement("thead", null,
                            react_1.default.createElement("tr", null,
                                react_1.default.createElement("th", null, "Date"),
                                react_1.default.createElement("th", null, "TxHash"),
                                react_1.default.createElement("th", null, "Address"),
                                react_1.default.createElement("th", null, "Value"))),
                        react_1.default.createElement("tbody", null, this.state.txs.map((v, index) => (react_1.default.createElement("tr", { key: index },
                            react_1.default.createElement("td", null, new Date(v.timeStamp * 1000).toLocaleString()),
                            react_1.default.createElement(OverflowTd, null,
                                react_1.default.createElement("a", { target: "_blank", href: `https://rinkeby.etherscan.io/tx/${v.hash}` }, v.hash)),
                            react_1.default.createElement(OverflowTd, null, v.from),
                            react_1.default.createElement("td", null, v.value))))))))));
    }
}
exports.default = Wallet;
//# sourceMappingURL=Wallet.js.map
});
___scope___.file("client/components/shared/table.jsx", function(exports, require, module, __filename, __dirname){

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const react_emotion_1 = tslib_1.__importDefault(require("react-emotion"));
exports.Table = react_emotion_1.default('table')({
    borderCollapse: 'collapse',
    borderSpacing: 0,
    td: {
        color: '#2e3d3f',
        padding: '1rem .5rem',
    },
    th: {
        color: '#457b9d',
        padding: '.5rem',
    },
    tr: {
        borderBottom: '1px solid #b2bcb9',
    },
    width: '100%',
});
//# sourceMappingURL=table.js.map
});
___scope___.file("client/components/WalletList.jsx", function(exports, require, module, __filename, __dirname){

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const react_1 = tslib_1.__importDefault(require("react"));
const react_emotion_1 = tslib_1.__importDefault(require("react-emotion"));
const react_redux_1 = require("react-redux");
const fuse_react_1 = require("fuse-react");
const recompact_1 = require("recompact");
const fetch_hoc_1 = tslib_1.__importDefault(require("fetch-hoc"));
const layout_1 = require("./shared/layout");
const typography_1 = require("./shared/typography");
const WalletList = ({ wallets, payData }) => react_1.default.createElement(react_1.default.Fragment, null,
    react_1.default.createElement(layout_1.Header, { to: '/' }),
    react_1.default.createElement(layout_1.Container, null,
        react_1.default.createElement(layout_1.Column, { style: { maxWidth: '32rem' } },
            react_1.default.createElement(layout_1.Row, null,
                react_1.default.createElement(typography_1.H1, null, "Choose a wallet")),
            wallets.map((v) => (react_1.default.createElement(LinkUndecorated, { to: `/txCreation/ftm/${v.address}`, key: v.address },
                react_1.default.createElement(WalletLinkContainer, null,
                    react_1.default.createElement("strong", null, "FTM"),
                    react_1.default.createElement("span", null,
                        v.address,
                        " ",
                        react_1.default.createElement("mark", null, v.balance)))))))));
const LinkUndecorated = react_emotion_1.default(fuse_react_1.Link)({
    color: '#457b9d',
    textDecoration: 'none',
});
const WalletLinkContainer = react_emotion_1.default(layout_1.Row)({
    ':hover': {
        cursor: 'pointer',
        span: {
            background: '#a8dadc',
            color: '#457b9d',
        },
        strong: {
            background: '#59c9d3',
        },
    },
    span: {
        alignItems: 'center',
        background: '#457b9d',
        color: '#f1faee',
        display: 'flex',
        justifyContent: 'space-between',
        marginLeft: '-2rem',
        mark: {
            background: 'transparent',
            textAlign: 'right',
        },
        padding: '1rem 1rem 1rem 3rem',
        position: 'relative',
        width: '100%',
        zIndex: 1,
    },
    strong: {
        alignItems: 'center',
        background: '#1d3557',
        borderRadius: '100%',
        color: '#f1faee',
        display: 'flex',
        height: '3.3rem',
        justifyContent: 'center',
        textTransform: 'uppercase',
        width: '4rem',
        zIndex: 2,
    },
});
const withFetch = fetch_hoc_1.default('http://localhost:4443/addresses');
const withConnect = react_redux_1.connect(({ wallet: { wallets, payData } }) => ({ wallets, payData }));
exports.default = recompact_1.compose(withFetch, withConnect)(WalletList);
//# sourceMappingURL=WalletList.js.map
});
___scope___.file("client/components/Webrtc/WebrtcServer.jsx", function(exports, require, module, __filename, __dirname){

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const react_1 = tslib_1.__importDefault(require("react"));
const react_redux_1 = require("react-redux");
const qrcode_react_1 = tslib_1.__importDefault(require("qrcode.react"));
const layout_1 = require("../shared/layout");
const typography_1 = require("../shared/typography");
const actions_1 = require("../../actions");
const constants_1 = require("../../constants");
const webrtc_1 = require("../../helpers/webrtc");
class WebrtcServer extends react_1.default.Component {
    constructor() {
        super(...arguments);
        this.state = {
            sid: null,
        };
        this.componentDidMount = async () => {
            const { initWebrtcConnaction, webrtc } = this.props;
            const offer = await webrtc.createOffer();
            const ws = new WebSocket(constants_1.handshakeServerUrl);
            ws.addEventListener('open', () => {
                console.log('opened!');
                ws.send(JSON.stringify({ jsonrpc: '2.0', id: 1, method: 'offer', params: { offer: offer.sdp } }));
            });
            ws.addEventListener('message', async (data) => {
                console.log(`message: ${data.data}`);
                const json = JSON.parse(data.data.toString());
                if (json.id === 1) {
                    this.setState({ sid: json.result.sid });
                }
                if (json.method === 'ice') {
                    webrtc.pushIceCandidate(json.params.ice);
                }
                if (json.method === 'answer') {
                    webrtc.on('ice', (ice) => {
                        ws.send(JSON.stringify({ jsonrpc: '2.0', id: 2, method: 'ice', params: { ice } }));
                    });
                    await webrtc.pushAnswer({ type: 'answer', sdp: json.params.answer });
                    console.log('wait connection');
                    await webrtc.waitConnection();
                    initWebrtcConnaction();
                    webrtc.dataChannel.send(webrtc_1.getWalletList());
                    ws.close();
                }
            });
        };
    }
    render() {
        const { sid } = this.state;
        return (react_1.default.createElement(react_1.default.Fragment, null, react_1.default.createElement(layout_1.Header, { to: '/' }), react_1.default.createElement(layout_1.Container, null, react_1.default.createElement(layout_1.Centered, null, react_1.default.createElement(layout_1.Row, null, sid && react_1.default.createElement(layout_1.Column, null, react_1.default.createElement(typography_1.H1, null, "Scan session id"), react_1.default.createElement(qrcode_react_1.default, { value: webrtc_1.webrtcLogin(sid), renderAs: 'svg', style: { width: '100%', height: '100%' } })))))));
    }
}
exports.default = react_redux_1.connect((state) => ({ webrtc: state.webrtc.webrtc }), { initWebrtcConnaction: actions_1.initWebrtcConnaction })(WebrtcServer);
//# sourceMappingURL=WebrtcServer.js.map
});
___scope___.file("client/components/TxView.jsx", function(exports, require, module, __filename, __dirname){

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const react_1 = tslib_1.__importDefault(require("react"));
const layout_1 = require("../components/shared/layout");
const typography_1 = require("./shared/typography");
const react_redux_1 = require("react-redux");
const TxView = ({ error, lastTransaction: tx }) => {
    return (react_1.default.createElement(react_1.default.Fragment, null,
        react_1.default.createElement(layout_1.Header, { to: '/' }),
        react_1.default.createElement(layout_1.Container, null,
            react_1.default.createElement(layout_1.Centered, null,
                react_1.default.createElement(layout_1.Column, null,
                    react_1.default.createElement(typography_1.H1, null, "Tx sent result"),
                    error
                        ? (react_1.default.createElement(typography_1.H2, null, error))
                        : (react_1.default.createElement(react_1.default.Fragment, null,
                            react_1.default.createElement(typography_1.H2, null,
                                "To : ",
                                tx.to),
                            react_1.default.createElement(typography_1.H2, null,
                                "Tx hash : ",
                                tx.transactionHash))))))));
};
exports.default = react_redux_1.connect((state) => state.webrtc)(TxView);
//# sourceMappingURL=TxView.js.map
});
___scope___.file("client/components/Pay.jsx", function(exports, require, module, __filename, __dirname){

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const react_1 = tslib_1.__importDefault(require("react"));
const fuse_react_1 = require("fuse-react");
const layout_1 = require("../components/shared/layout");
const buttons_1 = require("./shared/buttons");
const typography_1 = require("./shared/typography");
const actions_1 = require("../actions");
const react_redux_1 = require("react-redux");
const Pay = ({ match: { params: { address } }, setPayData }) => {
    //TODO: rewrite it
    const urlParams = new URLSearchParams(window.location.search);
    const amount = urlParams.get('amount');
    const data = urlParams.get('data');
    setPayData({ to: address, amount, data });
    return (react_1.default.createElement(react_1.default.Fragment, null,
        react_1.default.createElement(layout_1.Header, { to: '/' }),
        react_1.default.createElement(layout_1.Container, null,
            react_1.default.createElement(layout_1.Centered, null,
                react_1.default.createElement(layout_1.Column, null,
                    react_1.default.createElement(typography_1.H1, null, "Tx sent data"),
                    react_1.default.createElement(typography_1.H2, null,
                        "Address: ",
                        address),
                    react_1.default.createElement(typography_1.H2, null,
                        "Ammount: ",
                        amount),
                    react_1.default.createElement(typography_1.H2, null,
                        "Data: ",
                        data)),
                react_1.default.createElement(fuse_react_1.Link, { to: '/login' },
                    react_1.default.createElement(buttons_1.ButtonBase, null, "Login and pay"))))));
};
exports.default = react_redux_1.connect((state) => state.webrtc, { setPayData: actions_1.setPayData })(Pay);
//# sourceMappingURL=Pay.js.map
});
___scope___.file("client/components/PayToAddress.jsx", function(exports, require, module, __filename, __dirname){

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const react_1 = tslib_1.__importDefault(require("react"));
const layout_1 = require("../components/shared/layout");
const buttons_1 = require("./shared/buttons");
const typography_1 = require("./shared/typography");
const react_powerplug_1 = require("react-powerplug");
const web3_1 = tslib_1.__importDefault(require("web3"));
const layout_2 = require("./shared/layout");
const inputs_1 = require("./shared/inputs");
const webrtc_1 = require("./../helpers/webrtc");
// const tx = {
//   gasPrice: Web3.utils.toWei(value.gasPrice, 'gwei'),
//   to: value.to,
//   value: Web3.utils.toWei(value.amount),
//   data: value.data,
//   callback: value.callback,
//   blockchain: 'ftm'
// }
class PayToAddress extends react_1.default.Component {
    constructor() {
        super(...arguments);
        this.state = {
            blockChainData: {
                avgWait: 0
            },
            blockChainPrice: 0
        };
    }
    componentDidMount() {
        fetch(`https://ethgasstation.info/json/ethgasAPI.json`)
            .then((res) => res.json())
            .then((json) => this.setState({ blockChainData: json }));
        fetch(`https://api.coinmarketcap.com/v1/ticker/ethereum/`)
            .then((res) => res.json())
            .then((json) => this.setState({ blockChainPrice: json[0].price_usd }));
    }
    render() {
        const { match: { params: { address } } } = this.props;
        const { blockChainData, blockChainPrice } = this.state;
        const urlParams = new URLSearchParams(window.location.search);
        const amount = urlParams.get('amount');
        const data = urlParams.get('data');
        const callback = urlParams.get('callback');
        const initialValue = {
            gasPrice: "1",
            to: address,
            amount: amount,
            data: data,
            callback: callback,
            blockchain: 'eth',
        };
        return (react_1.default.createElement(react_1.default.Fragment, null,
            react_1.default.createElement(layout_1.Header, { to: '/' }),
            react_1.default.createElement(layout_1.Container, null,
                react_1.default.createElement(layout_1.Centered, { style: { maxWidth: '80vw' } },
                    react_1.default.createElement(react_powerplug_1.Form, { initial: initialValue }, ({ input, values }) => (react_1.default.createElement("form", { onSubmit: (e) => {
                            e.preventDefault();
                            const command = webrtc_1.payToAddress(values);
                            console.log(command);
                            window.location.href = `coldcrypto://connect?qr=${command}`;
                        } },
                        react_1.default.createElement(layout_1.Column, null,
                            react_1.default.createElement(typography_1.H1, null, "Tx sent data"),
                            react_1.default.createElement(typography_1.H2, null,
                                "Address: ",
                                address),
                            react_1.default.createElement(typography_1.H2, null,
                                "Amount: ",
                                amount),
                            react_1.default.createElement(typography_1.H2, null,
                                "Data: ",
                                data),
                            react_1.default.createElement(layout_2.Row, null,
                                react_1.default.createElement("span", null,
                                    "Gas price ",
                                    values.gasPrice,
                                    " GWEI"),
                                react_1.default.createElement(inputs_1.TextInput, Object.assign({ type: 'range' }, input('gasPrice').bind, { min: '1', max: '100' })),
                                react_1.default.createElement("span", null,
                                    " ",
                                    (Number(web3_1.default.utils.fromWei(values.gasPrice, "gwei")) * 21000 * Number(blockChainPrice)).toFixed(3),
                                    " USD"),
                                react_1.default.createElement("span", { style: { minWidth: '5rem', marginLeft: '.5rem' } },
                                    " ",
                                    `< ${(Number(blockChainData.avgWait) * Number(values.gasPrice)).toFixed(2)} min`)),
                            react_1.default.createElement(buttons_1.ButtonBase, { type: 'submit' }, "Pay")))))))));
    }
}
exports.default = PayToAddress;
//# sourceMappingURL=PayToAddress.js.map
});
return ___scope___.entry = "client/index.jsx";
});
FuseBox.pkg("events", {}, function(___scope___){
___scope___.file("index.js", function(exports, require, module, __filename, __dirname){

// Copyright Joyent, Inc. and other Node contributors.
//
// Permission is hereby granted, free of charge, to any person obtaining a
// copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the
// following conditions:
//
// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
// OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN
// NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
// USE OR OTHER DEALINGS IN THE SOFTWARE.
if (FuseBox.isServer) {
	module.exports = global.require("events");
} else {
	function EventEmitter() {
		this._events = this._events || {};
		this._maxListeners = this._maxListeners || undefined;
	}
	module.exports = EventEmitter;

	// Backwards-compat with node 0.10.x
	EventEmitter.EventEmitter = EventEmitter;

	EventEmitter.prototype._events = undefined;
	EventEmitter.prototype._maxListeners = undefined;

	// By default EventEmitters will print a warning if more than 10 listeners are
	// added to it. This is a useful default which helps finding memory leaks.
	EventEmitter.defaultMaxListeners = 10;

	// Obviously not all Emitters should be limited to 10. This function allows
	// that to be increased. Set to zero for unlimited.
	EventEmitter.prototype.setMaxListeners = function(n) {
		if (!isNumber(n) || n < 0 || isNaN(n)) throw TypeError("n must be a positive number");
		this._maxListeners = n;
		return this;
	};

	EventEmitter.prototype.emit = function(type) {
		var er, handler, len, args, i, listeners;

		if (!this._events) this._events = {};

		// If there is no 'error' event listener then throw.
		if (type === "error") {
			if (!this._events.error || (isObject(this._events.error) && !this._events.error.length)) {
				er = arguments[1];
				if (er instanceof Error) {
					throw er; // Unhandled 'error' event
				}
				throw TypeError('Uncaught, unspecified "error" event.');
			}
		}

		handler = this._events[type];

		if (isUndefined(handler)) return false;

		if (isFunction(handler)) {
			switch (arguments.length) {
				// fast cases
				case 1:
					handler.call(this);
					break;
				case 2:
					handler.call(this, arguments[1]);
					break;
				case 3:
					handler.call(this, arguments[1], arguments[2]);
					break;
				// slower
				default:
					args = Array.prototype.slice.call(arguments, 1);
					handler.apply(this, args);
			}
		} else if (isObject(handler)) {
			args = Array.prototype.slice.call(arguments, 1);
			listeners = handler.slice();
			len = listeners.length;
			for (i = 0; i < len; i++) listeners[i].apply(this, args);
		}

		return true;
	};

	EventEmitter.prototype.addListener = function(type, listener) {
		var m;

		if (!isFunction(listener)) throw TypeError("listener must be a function");

		if (!this._events) this._events = {};

		// To avoid recursion in the case that type === "newListener"! Before
		// adding it to the listeners, first emit "newListener".
		if (this._events.newListener) this.emit("newListener", type, isFunction(listener.listener) ? listener.listener : listener);

		if (!this._events[type])
			// Optimize the case of one listener. Don't need the extra array object.
			this._events[type] = listener;
		else if (isObject(this._events[type]))
			// If we've already got an array, just append.
			this._events[type].push(listener);
		// Adding the second element, need to change to array.
		else this._events[type] = [this._events[type], listener];

		// Check for listener leak
		if (isObject(this._events[type]) && !this._events[type].warned) {
			if (!isUndefined(this._maxListeners)) {
				m = this._maxListeners;
			} else {
				m = EventEmitter.defaultMaxListeners;
			}

			if (m && m > 0 && this._events[type].length > m) {
				this._events[type].warned = true;
				console.error(
					"(node) warning: possible EventEmitter memory " + "leak detected. %d listeners added. " + "Use emitter.setMaxListeners() to increase limit.",
					this._events[type].length
				);
				if (typeof console.trace === "function") {
					// not supported in IE 10
					console.trace();
				}
			}
		}

		return this;
	};

	EventEmitter.prototype.on = EventEmitter.prototype.addListener;

	EventEmitter.prototype.once = function(type, listener) {
		if (!isFunction(listener)) throw TypeError("listener must be a function");

		var fired = false;

		function g() {
			this.removeListener(type, g);

			if (!fired) {
				fired = true;
				listener.apply(this, arguments);
			}
		}

		g.listener = listener;
		this.on(type, g);

		return this;
	};

	// emits a 'removeListener' event iff the listener was removed
	EventEmitter.prototype.removeListener = function(type, listener) {
		var list, position, length, i;

		if (!isFunction(listener)) throw TypeError("listener must be a function");

		if (!this._events || !this._events[type]) return this;

		list = this._events[type];
		length = list.length;
		position = -1;

		if (list === listener || (isFunction(list.listener) && list.listener === listener)) {
			delete this._events[type];
			if (this._events.removeListener) this.emit("removeListener", type, listener);
		} else if (isObject(list)) {
			for (i = length; i-- > 0; ) {
				if (list[i] === listener || (list[i].listener && list[i].listener === listener)) {
					position = i;
					break;
				}
			}

			if (position < 0) return this;

			if (list.length === 1) {
				list.length = 0;
				delete this._events[type];
			} else {
				list.splice(position, 1);
			}

			if (this._events.removeListener) this.emit("removeListener", type, listener);
		}

		return this;
	};

	EventEmitter.prototype.removeAllListeners = function(type) {
		var key, listeners;

		if (!this._events) return this;

		// not listening for removeListener, no need to emit
		if (!this._events.removeListener) {
			if (arguments.length === 0) this._events = {};
			else if (this._events[type]) delete this._events[type];
			return this;
		}

		// emit removeListener for all listeners on all events
		if (arguments.length === 0) {
			for (key in this._events) {
				if (key === "removeListener") continue;
				this.removeAllListeners(key);
			}
			this.removeAllListeners("removeListener");
			this._events = {};
			return this;
		}

		listeners = this._events[type];

		if (isFunction(listeners)) {
			this.removeListener(type, listeners);
		} else if (listeners) {
			// LIFO order
			while (listeners.length) this.removeListener(type, listeners[listeners.length - 1]);
		}
		delete this._events[type];

		return this;
	};

	EventEmitter.prototype.listeners = function(type) {
		var ret;
		if (!this._events || !this._events[type]) ret = [];
		else if (isFunction(this._events[type])) ret = [this._events[type]];
		else ret = this._events[type].slice();
		return ret;
	};

	EventEmitter.prototype.listenerCount = function(type) {
		if (this._events) {
			var evlistener = this._events[type];

			if (isFunction(evlistener)) return 1;
			else if (evlistener) return evlistener.length;
		}
		return 0;
	};

	EventEmitter.listenerCount = function(emitter, type) {
		return emitter.listenerCount(type);
	};

	function isFunction(arg) {
		return typeof arg === "function";
	}

	function isNumber(arg) {
		return typeof arg === "number";
	}

	function isObject(arg) {
		return typeof arg === "object" && arg !== null;
	}

	function isUndefined(arg) {
		return arg === void 0;
	}
}

});
return ___scope___.entry = "index.js";
});
FuseBox.pkg("fusebox-hot-reload", {}, function(___scope___){
___scope___.file("index.js", function(exports, require, module, __filename, __dirname){

"use strict";
/**
 * @module listens to `source-changed` socket events and actions hot reload
 */
Object.defineProperty(exports, "__esModule", { value: true });
var Client = require("fusebox-websocket").SocketClient, bundleErrors = {}, outputElement = document.createElement("div"), styleElement = document.createElement("style"), minimizeToggleId = "fuse-box-toggle-minimized", hideButtonId = "fuse-box-hide", expandedOutputClass = "fuse-box-expanded-output", localStoragePrefix = "__fuse-box_";
function storeSetting(key, value) {
    localStorage[localStoragePrefix + key] = value;
}
function getSetting(key) {
    return localStorage[localStoragePrefix + key] === "true" ? true : false;
}
var outputInBody = false, outputMinimized = getSetting(minimizeToggleId), outputHidden = false;
outputElement.id = "fuse-box-output";
styleElement.innerHTML = "\n    #" + outputElement.id + ", #" + outputElement.id + " * {\n        box-sizing: border-box;\n    }\n    #" + outputElement.id + " {\n        z-index: 999999999999;\n        position: fixed;\n        top: 10px;\n        right: 10px;\n        width: 400px;\n        overflow: auto;\n        background: #fdf3f1;\n        border: 1px solid #eca494;\n        border-radius: 5px;\n        font-family: \"Helvetica Neue\", Helvetica, Arial, sans-serif;\n        box-shadow: 0px 3px 6px 1px rgba(0,0,0,.15);\n    }\n    #" + outputElement.id + "." + expandedOutputClass + " {\n        height: auto;\n        width: auto;\n        left: 10px;\n        max-height: calc(100vh - 50px);\n    }\n    #" + outputElement.id + " .fuse-box-errors {\n        display: none;\n    }\n    #" + outputElement.id + "." + expandedOutputClass + " .fuse-box-errors {\n        display: block;\n        border-top: 1px solid #eca494;\n        padding: 0 10px;\n    }\n    #" + outputElement.id + " button {\n        border: 1px solid #eca494;\n        padding: 5px 10px;\n        border-radius: 4px;\n        margin-left: 5px;\n        background-color: white;\n        color: black;\n        box-shadow: 0px 2px 2px 0px rgba(0,0,0,.05);\n    }\n    #" + outputElement.id + " .fuse-box-header {\n        padding: 10px;\n    }\n    #" + outputElement.id + " .fuse-box-header h4 {\n        display: inline-block;\n        margin: 4px;\n    }";
styleElement.type = "text/css";
document.getElementsByTagName("head")[0].appendChild(styleElement);
function displayBundleErrors() {
    var errorMessages = Object.keys(bundleErrors).reduce(function (allMessages, bundleName) {
        var bundleMessages = bundleErrors[bundleName];
        return allMessages.concat(bundleMessages.map(function (message) {
            var messageOutput = message
                .replace(/\n/g, "<br>")
                .replace(/\t/g, "&nbsp;&nbps;&npbs;&nbps;")
                .replace(/ /g, "&nbsp;");
            return "<pre>" + messageOutput + "</pre>";
        }));
    }, []), errorOutput = errorMessages.join("");
    if (errorOutput && !outputHidden) {
        outputElement.innerHTML = "\n        <div class=\"fuse-box-header\" style=\"\">\n            <h4 style=\"\">Fuse Box Bundle Errors (" + errorMessages.length + "):</h4>\n            <div style=\"float: right;\">\n                <button id=\"" + minimizeToggleId + "\">" + (outputMinimized ? "Expand" : "Minimize") + "</button>\n                <button id=\"" + hideButtonId + "\">Hide</button>\n            </div>\n        </div>\n        <div class=\"fuse-box-errors\">\n            " + errorOutput + "\n        </div>\n        ";
        document.body.appendChild(outputElement);
        outputElement.className = outputMinimized ? "" : expandedOutputClass;
        outputInBody = true;
        document.getElementById(minimizeToggleId).onclick = function () {
            outputMinimized = !outputMinimized;
            storeSetting(minimizeToggleId, outputMinimized);
            displayBundleErrors();
        };
        document.getElementById(hideButtonId).onclick = function () {
            outputHidden = true;
            displayBundleErrors();
        };
    }
    else if (outputInBody) {
        document.body.removeChild(outputElement);
        outputInBody = false;
    }
}
exports.connect = function (port, uri, reloadFullPage) {
    if (FuseBox.isServer) {
        return;
    }
    port = port || window.location.port;
    var client = new Client({
        port: port,
        uri: uri
    });
    client.connect();
    client.on("page-reload", function (data) {
        return window.location.reload();
    });
    client.on("page-hmr", function (data) {
        FuseBox.flush();
        FuseBox.dynamic(data.path, data.content);
        if (FuseBox.mainFile) {
            try {
                FuseBox.import(FuseBox.mainFile);
            }
            catch (e) {
                if (typeof e === "string") {
                    if (/not found/.test(e)) {
                        return window.location.reload();
                    }
                }
                console.error(e);
            }
        }
    });
    client.on("source-changed", function (data) {
        console.info("%cupdate \"" + data.path + "\"", "color: #237abe");
        if (reloadFullPage) {
            return window.location.reload();
        }
        /**
         * If a plugin handles this request then we don't have to do anything
         **/
        for (var index = 0; index < FuseBox.plugins.length; index++) {
            var plugin = FuseBox.plugins[index];
            if (plugin.hmrUpdate && plugin.hmrUpdate(data)) {
                return;
            }
        }
        if (data.type === "hosted-css") {
            var fileId = data.path.replace(/^\//, "").replace(/[\.\/]+/g, "-");
            var existing = document.getElementById(fileId);
            if (existing) {
                existing.setAttribute("href", data.path + "?" + new Date().getTime());
            }
            else {
                var node = document.createElement("link");
                node.id = fileId;
                node.type = "text/css";
                node.rel = "stylesheet";
                node.href = data.path;
                document.getElementsByTagName("head")[0].appendChild(node);
            }
        }
        if (data.type === "js" || data.type === "css") {
            FuseBox.flush();
            FuseBox.dynamic(data.path, data.content);
            if (FuseBox.mainFile) {
                try {
                    FuseBox.import(FuseBox.mainFile);
                }
                catch (e) {
                    if (typeof e === "string") {
                        if (/not found/.test(e)) {
                            return window.location.reload();
                        }
                    }
                    console.error(e);
                }
            }
        }
    });
    client.on("error", function (error) {
        console.log(error);
    });
    client.on("bundle-error", function (_a) {
        var bundleName = _a.bundleName, message = _a.message;
        console.error("Bundle error in " + bundleName + ": " + message);
        var errorsForBundle = bundleErrors[bundleName] || [];
        errorsForBundle.push(message);
        bundleErrors[bundleName] = errorsForBundle;
        displayBundleErrors();
    });
    client.on("update-bundle-errors", function (_a) {
        var bundleName = _a.bundleName, messages = _a.messages;
        messages.forEach(function (message) { return console.error("Bundle error in " + bundleName + ": " + message); });
        bundleErrors[bundleName] = messages;
        displayBundleErrors();
    });
};

});
return ___scope___.entry = "index.js";
});
FuseBox.pkg("fusebox-websocket", {}, function(___scope___){
___scope___.file("index.js", function(exports, require, module, __filename, __dirname){

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var events = require("events");
var SocketClient = /** @class */ (function () {
    function SocketClient(opts) {
        opts = opts || {};
        var port = opts.port || window.location.port;
        var protocol = location.protocol === "https:" ? "wss://" : "ws://";
        var domain = location.hostname || "localhost";
        this.url = opts.host || "" + protocol + domain + ":" + port;
        if (opts.uri) {
            this.url = opts.uri;
        }
        this.authSent = false;
        this.emitter = new events.EventEmitter();
    }
    SocketClient.prototype.reconnect = function (fn) {
        var _this = this;
        setTimeout(function () {
            _this.emitter.emit("reconnect", { message: "Trying to reconnect" });
            _this.connect(fn);
        }, 5000);
    };
    SocketClient.prototype.on = function (event, fn) {
        this.emitter.on(event, fn);
    };
    SocketClient.prototype.connect = function (fn) {
        var _this = this;
        console.log("%cConnecting to fusebox HMR at " + this.url, "color: #237abe");
        setTimeout(function () {
            _this.client = new WebSocket(_this.url);
            _this.bindEvents(fn);
        }, 0);
    };
    SocketClient.prototype.close = function () {
        this.client.close();
    };
    SocketClient.prototype.send = function (eventName, data) {
        if (this.client.readyState === 1) {
            this.client.send(JSON.stringify({ event: eventName, data: data || {} }));
        }
    };
    SocketClient.prototype.error = function (data) {
        this.emitter.emit("error", data);
    };
    /** Wires up the socket client messages to be emitted on our event emitter */
    SocketClient.prototype.bindEvents = function (fn) {
        var _this = this;
        this.client.onopen = function (event) {
            console.log("%cConnected", "color: #237abe");
            if (fn) {
                fn(_this);
            }
        };
        this.client.onerror = function (event) {
            _this.error({ reason: event.reason, message: "Socket error" });
        };
        this.client.onclose = function (event) {
            _this.emitter.emit("close", { message: "Socket closed" });
            if (event.code !== 1011) {
                _this.reconnect(fn);
            }
        };
        this.client.onmessage = function (event) {
            var data = event.data;
            if (data) {
                var item = JSON.parse(data);
                _this.emitter.emit(item.type, item.data);
                _this.emitter.emit("*", item);
            }
        };
    };
    return SocketClient;
}());
exports.SocketClient = SocketClient;

});
return ___scope___.entry = "index.js";
});
FuseBox.import("fusebox-hot-reload").connect(4444, "", false)
var $fsmp$ = (function() {
	function loadRemoteScript(url) {
		return Promise.resolve().then(function() {
			if (FuseBox.isBrowser) {
				let d = document;
				var head = d.getElementsByTagName("head")[0];
				var target;
				if (/\.css$/.test(url)) {
					target = d.createElement("link");
					target.rel = "stylesheet";
					target.type = "text/css";
					target.href = url;
				} else {
					target = d.createElement("script");
					target.type = "text/javascript";
					target.src = url;
					target.async = true;
				}
				head.insertBefore(target, head.firstChild);
			}
		});
	}

	function request(url, cb) {
		if (FuseBox.isServer) {
			try {
				if (/^http(s)?\:/.test(url)) {
					return require("request")(url, function(error, response, body) {
						if (error) {
							return cb(error);
						}
						return cb(null, body, response.headers["content-type"]);
					});
				}
				if (/\.(js|json)$/.test(url)) {
					cb(null, require(url));
				} else {
					cb(
						null,
						require("fs")
							.readFileSync(require("path").join(__dirname, url))
							.toString()
					);
				}
			} catch (e) {
				cb(e);
			}
		} else {
			var request = new XMLHttpRequest();
			request.onreadystatechange = function() {
				var err;
				if (this.readyState == 4) {
					if (this.status !== 200) {
						err = { code: this.status, msg: this.statusText };
					}
					cb(err, this.responseText, request.getResponseHeader("Content-Type"));
				}
			};
			request.open("GET", url, true);
			request.setRequestHeader("X-Requested-With", "XMLHttpRequest");
			request.send();
		}
	}

	function evaluateModule(id, code) {
		var fn = new Function("module", "exports", code);
		var moduleExports = {};
		var moduleObject = { exports: moduleExports };
		fn(moduleObject, moduleExports);
		return moduleObject.exports;
	}

	return function(id) {
		return new Promise(function(resolve, reject) {
			if (FuseBox.exists(id)) {
				return resolve(FuseBox.import(id));
			}

			var isCSS = /\.css$/.test(id);
			if (FuseBox.isServer) {
				if (isCSS) {
					return reject("Can't load CSS on server!");
				}
			}
			// id.charCodeAt(4) = : which means http
			if (FuseBox.isBrowser) {
				if (name.charCodeAt(4) === 58 || name.charCodeAt(5) === 58 || isCSS) {
					return loadRemoteScript(id);
				}
			}
			var splitConfig = FuseBox.global("__fsbx__bundles__");

			if (splitConfig && splitConfig.bundles) {
				if (splitConfig.bundles[id]) {
					return resolve(FuseBox.import("~/" + splitConfig.bundles[id].main));
				}
			}

			request(id, function(error, contents, type) {
				if (error) {
					return reject(error);
				}
				var data;

				if (type) {
					if (/javascript/.test(type)) {
						data = evaluateModule(id, contents);
					} else if (/json/.test(type)) {
						data = JSON.parse(contents);
					} else if (!/javascript/.test(type)) {
						data = contents;
					} else {
						data = contents;
					}
				} else {
					data = contents;
				}

				return resolve(data);
			});
		});
	};
})();
if (FuseBox.isBrowser) {
	window.$fsmp$ = $fsmp$;
}


FuseBox.import("default/client/index.jsx");
FuseBox.main("default/client/index.jsx");
})
(function(e){function r(e){var r=e.charCodeAt(0),n=e.charCodeAt(1);if((m||58!==n)&&(r>=97&&r<=122||64===r)){if(64===r){var t=e.split("/"),i=t.splice(2,t.length).join("/");return[t[0]+"/"+t[1],i||void 0]}var o=e.indexOf("/");if(o===-1)return[e];var a=e.substring(0,o),f=e.substring(o+1);return[a,f]}}function n(e){return e.substring(0,e.lastIndexOf("/"))||"./"}function t(){for(var e=[],r=0;r<arguments.length;r++)e[r]=arguments[r];for(var n=[],t=0,i=arguments.length;t<i;t++)n=n.concat(arguments[t].split("/"));for(var o=[],t=0,i=n.length;t<i;t++){var a=n[t];a&&"."!==a&&(".."===a?o.pop():o.push(a))}return""===n[0]&&o.unshift(""),o.join("/")||(o.length?"/":".")}function i(e){var r=e.match(/\.(\w{1,})$/);return r&&r[1]?e:e+".js"}function o(e){if(m){var r,n=document,t=n.getElementsByTagName("head")[0];/\.css$/.test(e)?(r=n.createElement("link"),r.rel="stylesheet",r.type="text/css",r.href=e):(r=n.createElement("script"),r.type="text/javascript",r.src=e,r.async=!0),t.insertBefore(r,t.firstChild)}}function a(e,r){for(var n in e)e.hasOwnProperty(n)&&r(n,e[n])}function f(e){return{server:require(e)}}function u(e,n){var o=n.path||"./",a=n.pkg||"default",u=r(e);if(u&&(o="./",a=u[0],n.v&&n.v[a]&&(a=a+"@"+n.v[a]),e=u[1]),e)if(126===e.charCodeAt(0))e=e.slice(2,e.length),o="./";else if(!m&&(47===e.charCodeAt(0)||58===e.charCodeAt(1)))return f(e);var s=x[a];if(!s){if(m&&"electron"!==_.target)throw"Package not found "+a;return f(a+(e?"/"+e:""))}e=e?e:"./"+s.s.entry;var l,d=t(o,e),c=i(d),p=s.f[c];return!p&&c.indexOf("*")>-1&&(l=c),p||l||(c=t(d,"/","index.js"),p=s.f[c],p||"."!==d||(c=s.s&&s.s.entry||"index.js",p=s.f[c]),p||(c=d+".js",p=s.f[c]),p||(p=s.f[d+".jsx"]),p||(c=d+"/index.jsx",p=s.f[c])),{file:p,wildcard:l,pkgName:a,versions:s.v,filePath:d,validPath:c}}function s(e,r,n){if(void 0===n&&(n={}),!m)return r(/\.(js|json)$/.test(e)?h.require(e):"");if(n&&n.ajaxed===e)return console.error(e,"does not provide a module");var i=new XMLHttpRequest;i.onreadystatechange=function(){if(4==i.readyState)if(200==i.status){var n=i.getResponseHeader("Content-Type"),o=i.responseText;/json/.test(n)?o="module.exports = "+o:/javascript/.test(n)||(o="module.exports = "+JSON.stringify(o));var a=t("./",e);_.dynamic(a,o),r(_.import(e,{ajaxed:e}))}else console.error(e,"not found on request"),r(void 0)},i.open("GET",e,!0),i.send()}function l(e,r){var n=y[e];if(n)for(var t in n){var i=n[t].apply(null,r);if(i===!1)return!1}}function d(e){if(null!==e&&["function","object","array"].indexOf(typeof e)!==-1&&!e.hasOwnProperty("default"))return Object.isFrozen(e)?void(e.default=e):void Object.defineProperty(e,"default",{value:e,writable:!0,enumerable:!1})}function c(e,r){if(void 0===r&&(r={}),58===e.charCodeAt(4)||58===e.charCodeAt(5))return o(e);var t=u(e,r);if(t.server)return t.server;var i=t.file;if(t.wildcard){var a=new RegExp(t.wildcard.replace(/\*/g,"@").replace(/[.?*+^$[\]\\(){}|-]/g,"\\$&").replace(/@@/g,".*").replace(/@/g,"[a-z0-9$_-]+"),"i"),f=x[t.pkgName];if(f){var p={};for(var v in f.f)a.test(v)&&(p[v]=c(t.pkgName+"/"+v));return p}}if(!i){var g="function"==typeof r,y=l("async",[e,r]);if(y===!1)return;return s(e,function(e){return g?r(e):null},r)}var w=t.pkgName;if(i.locals&&i.locals.module)return i.locals.module.exports;var b=i.locals={},j=n(t.validPath);b.exports={},b.module={exports:b.exports},b.require=function(e,r){var n=c(e,{pkg:w,path:j,v:t.versions});return _.sdep&&d(n),n},m||!h.require.main?b.require.main={filename:"./",paths:[]}:b.require.main=h.require.main;var k=[b.module.exports,b.require,b.module,t.validPath,j,w];return l("before-import",k),i.fn.apply(k[0],k),l("after-import",k),b.module.exports}if(e.FuseBox)return e.FuseBox;var p="undefined"!=typeof ServiceWorkerGlobalScope,v="undefined"!=typeof WorkerGlobalScope,m="undefined"!=typeof window&&"undefined"!=typeof window.navigator||v||p,h=m?v||p?{}:window:global;m&&(h.global=v||p?{}:window),e=m&&"undefined"==typeof __fbx__dnm__?e:module.exports;var g=m?v||p?{}:window.__fsbx__=window.__fsbx__||{}:h.$fsbx=h.$fsbx||{};m||(h.require=require);var x=g.p=g.p||{},y=g.e=g.e||{},_=function(){function r(){}return r.global=function(e,r){return void 0===r?h[e]:void(h[e]=r)},r.import=function(e,r){return c(e,r)},r.on=function(e,r){y[e]=y[e]||[],y[e].push(r)},r.exists=function(e){try{var r=u(e,{});return void 0!==r.file}catch(e){return!1}},r.remove=function(e){var r=u(e,{}),n=x[r.pkgName];n&&n.f[r.validPath]&&delete n.f[r.validPath]},r.main=function(e){return this.mainFile=e,r.import(e,{})},r.expose=function(r){var n=function(n){var t=r[n].alias,i=c(r[n].pkg);"*"===t?a(i,function(r,n){return e[r]=n}):"object"==typeof t?a(t,function(r,n){return e[n]=i[r]}):e[t]=i};for(var t in r)n(t)},r.dynamic=function(r,n,t){this.pkg(t&&t.pkg||"default",{},function(t){t.file(r,function(r,t,i,o,a){var f=new Function("__fbx__dnm__","exports","require","module","__filename","__dirname","__root__",n);f(!0,r,t,i,o,a,e)})})},r.flush=function(e){var r=x.default;for(var n in r.f)e&&!e(n)||delete r.f[n].locals},r.pkg=function(e,r,n){if(x[e])return n(x[e].s);var t=x[e]={};return t.f={},t.v=r,t.s={file:function(e,r){return t.f[e]={fn:r}}},n(t.s)},r.addPlugin=function(e){this.plugins.push(e)},r.packages=x,r.isBrowser=m,r.isServer=!m,r.plugins=[],r}();return m||(h.FuseBox=_),e.FuseBox=_}(this))
//# sourceMappingURL=client.js.map?tm=1540654157566